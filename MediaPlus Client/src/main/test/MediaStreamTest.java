/*
 * This file is part of VLCJ.
 *
 * VLCJ is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VLCJ is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLCJ.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright 2009, 2010, 2011, 2012, 2013, 2014, 2015 Caprica Software Limited.
 */

import java.awt.Canvas;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import uk.co.caprica.vlcj.binding.internal.libvlc_media_t;
import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventListener;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.embedded.FullScreenStrategy;

/**
 * Test demonstrating the {@link EmbeddedMediaPlayerComponent} and how to tailor it by sub-classing
 * and overriding template methods.
 */
@SuppressWarnings("serial")
public class MediaStreamTest {

    /**
     * Media player component.
     */
    private final EmbeddedMediaPlayerComponent mediaPlayerComponent;

    /**
     * Application entry point.
     *
     * @param args
     */
    public static void main(String[] args) {

        final String mrl = "http://192.168.0.149:8080";

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MediaStreamTest().start(mrl);
            }
        });
    }

    /**
     * Create a new test.
     */
    private MediaStreamTest() {

        NativeDiscovery vlcFinder = new NativeDiscovery();
        boolean vlcFound = vlcFinder.discover();

        if(!vlcFound){
            System.out.println("VLC not found.");
            System.exit(1);
        }

        JFrame frame = new JFrame("HTTP Stream");



        // Create a sub-class of the embedded media player component (it does not
        // need to be an inner class, it could be a standalone top-level class) and
        // override one or more of the template methods to tailor the component to
        // your needs (this example implementation doesn't actually do anything
        // different)...
        mediaPlayerComponent = new EmbeddedMediaPlayerComponent() {
            @Override
            protected MediaPlayerFactory onGetMediaPlayerFactory() {
                return super.onGetMediaPlayerFactory();
            }

            @Override
            protected String[] onGetMediaPlayerFactoryArgs() {
                return super.onGetMediaPlayerFactoryArgs();
            }

            @Override
            protected FullScreenStrategy onGetFullScreenStrategy() {
                return super.onGetFullScreenStrategy();
            }

            @Override
            protected Canvas onGetCanvas() {
                return super.onGetCanvas();
            }

            @Override
            protected void onBeforeRelease() {
                super.onBeforeRelease();
            }

            @Override
            protected void onAfterRelease() {
                super.onAfterRelease();
            }

            public void buffering(MediaPlayer mediaPlayer, float newCache) {
                System.out.println("Buffering " + newCache);
            }

            // Override whatever listener methods you're interested in...

            @Override
            public void videoOutput(MediaPlayer mediaPlayer, int newCount) {
                super.videoOutput(mediaPlayer, newCount);
            }

            @Override
            public void playing(MediaPlayer mediaPlayer) {
                super.playing(mediaPlayer);
            }

            @Override
            public void error(MediaPlayer mediaPlayer) {
                super.error(mediaPlayer);
            }
        };

        mediaPlayerComponent.getMediaPlayer().setRepeat(true);

        mediaPlayerComponent.getMediaPlayer().addMediaPlayerEventListener(new MediaPlayerEventListener() {
            @Override
            public void mediaChanged(MediaPlayer mediaPlayer, libvlc_media_t media, String mrl) {


            }

            @Override
            public void opening(MediaPlayer mediaPlayer) {

            }

            @Override
            public void buffering(MediaPlayer mediaPlayer, float newCache) {
                System.out.println("Buffering " + newCache);
            }

            @Override
            public void playing(MediaPlayer mediaPlayer) {

            }

            @Override
            public void paused(MediaPlayer mediaPlayer) {

            }

            @Override
            public void stopped(MediaPlayer mediaPlayer) {

            }

            @Override
            public void forward(MediaPlayer mediaPlayer) {

            }

            @Override
            public void backward(MediaPlayer mediaPlayer) {

            }

            @Override
            public void finished(MediaPlayer mediaPlayer) {

            }

            @Override
            public void timeChanged(MediaPlayer mediaPlayer, long newTime) {

            }

            @Override
            public void positionChanged(MediaPlayer mediaPlayer, float newPosition) {

            }

            @Override
            public void seekableChanged(MediaPlayer mediaPlayer, int newSeekable) {

            }

            @Override
            public void pausableChanged(MediaPlayer mediaPlayer, int newPausable) {

            }

            @Override
            public void titleChanged(MediaPlayer mediaPlayer, int newTitle) {

            }

            @Override
            public void snapshotTaken(MediaPlayer mediaPlayer, String filename) {

            }

            @Override
            public void lengthChanged(MediaPlayer mediaPlayer, long newLength) {

            }

            @Override
            public void videoOutput(MediaPlayer mediaPlayer, int newCount) {

            }

            @Override
            public void scrambledChanged(MediaPlayer mediaPlayer, int newScrambled) {

            }

            @Override
            public void elementaryStreamAdded(MediaPlayer mediaPlayer, int type, int id) {

            }

            @Override
            public void elementaryStreamDeleted(MediaPlayer mediaPlayer, int type, int id) {

            }

            @Override
            public void elementaryStreamSelected(MediaPlayer mediaPlayer, int type, int id) {

            }

            @Override
            public void error(MediaPlayer mediaPlayer) {

            }

            @Override
            public void mediaMetaChanged(MediaPlayer mediaPlayer, int metaType) {

            }

            @Override
            public void mediaSubItemAdded(MediaPlayer mediaPlayer, libvlc_media_t subItem) {

            }

            @Override
            public void mediaDurationChanged(MediaPlayer mediaPlayer, long newDuration) {

            }

            @Override
            public void mediaParsedChanged(MediaPlayer mediaPlayer, int newStatus) {

            }

            @Override
            public void mediaFreed(MediaPlayer mediaPlayer) {

            }

            @Override
            public void mediaStateChanged(MediaPlayer mediaPlayer, int newState) {

            }

            @Override
            public void newMedia(MediaPlayer mediaPlayer) {

            }

            @Override
            public void subItemPlayed(MediaPlayer mediaPlayer, int subItemIndex) {

            }

            @Override
            public void subItemFinished(MediaPlayer mediaPlayer, int subItemIndex) {

            }

            @Override
            public void endOfSubItems(MediaPlayer mediaPlayer) {

            }
        });

        mediaPlayerComponent.getMediaPlayer().setVolume(100);
///        mediaPlayerComponent.getMediaPlayer().play();

        frame.setContentPane(mediaPlayerComponent);

        frame.setLocation(100, 100);
        frame.setSize(1050, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    /**
     * Start playing a movie.
     *
     * @param mrl mrl
     */
    private void start(String mrl) {
        mediaPlayerComponent.getMediaPlayer().playMedia(mrl);
    }
}