package com.mediaplus;

import uk.co.caprica.vlcj.player.MediaPlayer;
import com.mediaplus.io.Client;
import com.mediaplus.io.Preferences;
import com.mediaplus.media.MediaHandler;

/**
 * Created by Deividas on 14/11/2014.
 */
public class BaseObject {

	public static Main bootstrap;
	public static Renderer renderer;
	public static Preferences pref;
    public static MediaPlayer player;
    public static Client client;
    public static MediaHandler mediaHandler;
}
