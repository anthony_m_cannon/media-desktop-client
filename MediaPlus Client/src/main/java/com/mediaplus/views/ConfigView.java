package com.mediaplus.views;

import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.mediaplus.controllers.ConfigController;
import com.mediaplus.types.ButtonCommandType;
import com.mediaplus.types.HomeCategoryType;
import com.mediaplus.types.HomeLayoutType;

public class ConfigView extends View {
    private static final long serialVersionUID = 1L;

    private JLabel ipHeading, portHeading;
    private JLabel layoutHeading, categoryHeading;
    private JTextField ipField, portField;
    private JButton connectBtn;
    private JComboBox<String> category, layout;
    private Checkbox fullScreenBox;
    
    private String ipTxt, portTxt;
    private boolean fullScreenState;
    private int layoutId, categoryId;
    
    private int margin;
    private final String[] categoryList, layoutList;

    private JLabel connectionSettingsLabel;
    private JLabel layoutSettingsLabel;

    public ConfigView(int width, int height, String ipTxt, String portTxt, Image logo, boolean fullScreenState, int layoutId, int categoryId) {
        super(width, height);
        
        layoutList = HomeLayoutType.getAll();
        categoryList = HomeCategoryType.getAll();
        margin = 10;
        this.ipTxt = ipTxt;
        this.portTxt = portTxt;
        this.fullScreenState = fullScreenState;
        this.layoutId = layoutId;
        this.categoryId = categoryId;
        
        finalise();
    }

    @Override
    public void createElements() {
    	

        // Fields
        ipHeading = new JLabel("IP:");
        portHeading = new JLabel("Port:");
        ipField = new JTextField();
        portField = new JTextField();
        
        // Buttons
        connectBtn = new JButton();

        // Drop Boxes
    	layoutHeading = new JLabel("Default Layout:");
    	categoryHeading = new JLabel("Default Category:");
        category = new JComboBox<String>(categoryList);
        layout = new JComboBox<String>(layoutList);

        // Check Boxes
        fullScreenBox = new Checkbox(" Full screen");

        connectionSettingsLabel = new JLabel("Connection Settings");
        layoutSettingsLabel = new JLabel("Layout Settings");

    }

    @Override
    public void setElementsAdditionalInfo() {
        category.setSelectedIndex(categoryId);
        layout.setSelectedIndex(layoutId);
        category.setForeground(Color.black);
        layout.setForeground(Color.black);

        layoutHeading.setForeground(Color.white);
        categoryHeading.setForeground(Color.white);
        
        ipHeading.setForeground(Color.white);
        portHeading.setForeground(Color.white);
        
        ipField.setForeground(Color.white);
        portField.setForeground(Color.white);
        ipField.setBackground(Color.gray);
        portField.setBackground(Color.gray);
        
        ipField.setText(ipTxt);
        portField.setText(portTxt);
        
        fullScreenBox.setState(fullScreenState);
        fullScreenBox.setBackground(null);
        fullScreenBox.setForeground(Color.white);
        
        connectBtn.setText("Connect");
    }

    @Override
    public void setElementsSize() {
        // Fields
        ipHeading.setSize(portHeading.getPreferredSize());
        portHeading.setSize(portHeading.getPreferredSize());
        ipField.setSize(200, portHeading.getPreferredSize().height);
        portField.setSize(200, portHeading.getPreferredSize().height);

        // Buttons
        connectBtn.setSize(connectBtn.getPreferredSize());

        // Drop Boxes
    	layoutHeading.setSize(layoutHeading.getPreferredSize());
    	categoryHeading.setSize(categoryHeading.getPreferredSize());
        category.setSize(category.getPreferredSize());
        layout.setSize(category.getPreferredSize());

        // Check Boxes
        fullScreenBox.setSize((int) category.getPreferredSize().getWidth() + 16, 20);


        Font newFont1 = connectionSettingsLabel.getFont();
        Font newFont2 = new Font(newFont1.getFamily(), Font.PLAIN, 16);

        connectionSettingsLabel.setFont(newFont2);
        layoutSettingsLabel.setFont(newFont2);
        connectionSettingsLabel.setSize(connectionSettingsLabel.getPreferredSize());
        layoutSettingsLabel.setSize(layoutSettingsLabel.getPreferredSize());
    }

    @Override
    public void setElementsLocation() {
    	int ipXPos = (int) Math.round((windowWidth - ipField.getWidth()) / (8.0/3.0)) ;
    	int ipYPos = windowHeight / 4;
    	int portXPos = ipXPos;
    	int portYPos = (int) (ipYPos + portField.getHeight() + margin);

    	int conXPos = portXPos + ((portField.getWidth() - connectBtn.getWidth()) / 2);
    	int conYPos = portYPos + portField.getHeight() + margin;
    	
    	int ipHXPos = ipXPos - ipHeading.getWidth() - margin;
    	int ipHYPos = ipYPos;
    	int portHXPos = portXPos - portHeading.getWidth() - margin;
    	int portHYPos = portYPos;
    	
    	int catXPos = (int) Math.round((windowWidth - category.getWidth()) / (8.0/5.0));
    	int catYPos = windowHeight / 4;
    	int layXPos = catXPos;
    	int layYPos = catYPos + category.getHeight() + margin;
    	
    	int catHXPos = catXPos - categoryHeading.getWidth() - margin;
    	int catHYPos = catYPos;
    	int layHXPos = layXPos - layoutHeading.getWidth() - margin;
    	int layHYPos = layYPos;
    	
    	int fullYPos = layYPos + layout.getHeight() + margin;

        // Fields
        ipHeading.setLocation(ipHXPos, ipHYPos);
        portHeading.setLocation(portHXPos, portHYPos);
        ipField.setLocation(ipXPos, ipYPos);
        portField.setLocation(portXPos, portYPos);

        connectionSettingsLabel.setForeground(Color.WHITE);
        connectionSettingsLabel.setLocation(ipHXPos + ((ipHeading.getWidth() / 2) + (connectionSettingsLabel.getWidth() / 2)), ipHYPos - 60);

        // Buttons
        connectBtn.setLocation(conXPos, conYPos);

        // Drop Boxes
    	layoutHeading.setLocation(layHXPos, layHYPos);
    	categoryHeading.setLocation(catHXPos, catHYPos);
        category.setLocation(catXPos, catYPos);
        layout.setLocation(layXPos, layYPos);

        layoutSettingsLabel.setForeground(Color.WHITE);
        layoutSettingsLabel.setLocation(layXPos + ((layoutHeading.getWidth() / 2) - (layoutSettingsLabel.getWidth() / 2)), ipHYPos - 60);

        // Check Boxes
        fullScreenBox.setLocation(layout.getX(), fullYPos);
    }

    @Override
    public void addElements() {

        // Fields
        add(ipHeading);
        add(portHeading);
        add(ipField);
        add(portField);

        // Buttons
        add(connectBtn);

        // Drop Boxes
        add(category);
        add(layout);
        add(layoutHeading);
        add(categoryHeading);

        // Check Boxes
        add(fullScreenBox);
        add(connectionSettingsLabel);
        add(layoutSettingsLabel);
    }

    public void addActionListeners(ConfigController listener) {

        category.setActionCommand(ButtonCommandType.CATEGORY.name());
        layout.setActionCommand(ButtonCommandType.LAYOUT.name());
        category.addActionListener(listener);
        layout.addActionListener(listener);

        fullScreenBox.addItemListener(listener);

        connectBtn.setActionCommand(ButtonCommandType.CONNECT.name());
        connectBtn.addActionListener(listener);
    }
    
    // gets
    public String getIpField() {
		return ipField.getText().toString();
	}
    
    public String getPortField() {
		return portField.getText().toString();
	}
    
    public boolean getFullScreen() {
		return fullScreenBox.getState();
	}
    
    public int getLayoutId() {
		return layout.getSelectedIndex();
	}
    
    public int getCategoryId() {
		return category.getSelectedIndex();
	}
    
    @Override
    public void paint(Graphics g) {
    	super.paint(g); // paint elements
    	
        Graphics2D g2 = (Graphics2D) g;
        
        g2.setColor(Color.white);
        g2.drawLine(windowWidth / 2, connectionSettingsLabel.getY() - 25, windowWidth / 2, fullScreenBox.getY() + 25);
    }
}
