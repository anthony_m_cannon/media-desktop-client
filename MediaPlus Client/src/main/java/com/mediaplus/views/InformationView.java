package com.mediaplus.views;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.util.Arrays;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.mediaplus.BaseObject;
import com.mediaplus.controllers.InformationController;
import com.mediaplus.io.Resources;
import com.mediaplus.media.Film;
import com.mediaplus.types.ButtonCommandType;

public class InformationView extends View {
	private static final long serialVersionUID = 1L;
	
    private JPanel centerPanel;
    private JLabel posterLabel;
    private JLabel infoLabel;
    private Film film;

    private Image posterImage;
    private int posterX = 20;
    private int posterY = 20;
    private int infoX;
    private int infoY;
    private int infoXOffset = 20;
    private int streamButtonHeight = 50;
    private final String HTML_HEAD = "<html><body>";
    private final String HTML_FOOT = "</html></body>";
    private String movieFileName = "";
    private String movieName = "";
    private int movieYear = 0;
    private String movieCountry = "";
    private int movieRuntime = 0;
    private String movieCategory = "";
    private double movieImdbRating = 0.0;
    private double movieRottRating = 0.0;
    private String moviePlot = "";
    private String movieDirectors;
    private String movieActors;

    private JButton streamButton;

    public InformationView(int width, int height) {
        super(width, height);
        posterImage = Resources.IMAGE_PICTURE_NOT_AVAILABLE;

        finalise();
    }

    @Override
    public void createElements() {
        this.setLayout(null);
        centerPanel = new JPanel();

        posterLabel = new JLabel();
        infoLabel = new JLabel();
        streamButton = new JButton();
    }

    @Override
    public void setElementsAdditionalInfo() {
        centerPanel.setLayout(null);
        centerPanel.setBackground(Color.BLACK);

        setButtonImage(streamButton, Resources.IMAGE_BUTTON_STREAM, Resources.IMAGE_BUTTON_STREAM_HOVER);

        ImageIcon poster = new ImageIcon(posterImage);
        posterLabel.setIcon(poster);

        infoLabel.setForeground(Color.WHITE);
        Font temp = infoLabel.getFont();
        Font font = new Font(temp.getFamily(), Font.PLAIN, 18);
        infoLabel.setFont(font);
        infoLabel.setText(HTML_HEAD
                + "<br/>" + (!movieName.equals("") ? movieName : movieFileName) + "<br/>"
                + (movieYear != 0 ? " (" + movieYear + ")<br/>" : "")
                + (movieYear != 0 ? movieCountry + "<br/>" : "")
                + (movieRuntime != 0 ? movieRuntime + " mins<br/>" : "")
                + (!movieCategory.equals("") ? movieCategory + "<br/>" : "")
                + (movieImdbRating != 0.0 ? "IMDB Rating - " + movieImdbRating + "<br/>" : "")
                + (movieRottRating != 0.0 ? "Rotten tomatoes rating - " + movieRottRating + "<br/><br/>" : "<br/>")
                + (moviePlot != null ? moviePlot + "<br/><br/>" : "<br/>")
                + (movieDirectors != null ? "Director(s): " + movieDirectors + "<br/>" : "")
                + (movieActors != null ? "Actor(s): " + movieActors + "<br/>" : "")
                + HTML_FOOT);
        infoLabel.setBounds(infoX, infoY, (int) 500, (int) infoLabel.getPreferredSize().getHeight() + 20);
    }

    @Override
    public void setElementsSize() {
        posterLabel.setSize(posterImage.getWidth(null), posterImage.getHeight(null));
        streamButton.setSize(streamButton.getPreferredSize());
        centerPanel.setSize(windowWidth, windowHeight);
    }

    @Override
    public void setElementsLocation() {
        posterLabel.setBounds(posterX, posterY, posterImage.getWidth(null), posterImage.getHeight(null));
        System.out.println(posterY + posterImage.getHeight(null));
        streamButton.setLocation((posterX/2) + (streamButton.getWidth()/2), posterY + posterImage.getHeight(null) + 2);

        infoX = posterX + posterImage.getWidth(null) + infoXOffset;
        infoY = posterY;
        
        infoLabel.setLocation(infoX, infoY);
        infoLabel.setSize(infoLabel.getPreferredSize());
    }

    @Override
    public void addElements() {

        centerPanel.add(posterLabel);
        centerPanel.add(infoLabel);
        centerPanel.add(streamButton);
        add(centerPanel);
    }

    public void setFilm(String title) {
        System.out.println("LOOKING AT " + title);
        film = BaseObject.mediaHandler.getFilmByFileName(title);

        movieFileName = title;

        System.out.println("SHOWING " + film + "");
        System.out.println(film == null ? "null" : "not null");
        if(film != null) {
            posterImage = film.getCover();
            movieName = film.getTitle();
            movieYear = film.getYear();
            movieCountry = film.getCountry();
            movieCategory = film.getGenre();
            movieImdbRating = film.getIMDBRating();
            movieRottRating = film.getRTRating();
            movieRuntime = film.getRunTime();
            moviePlot = film.getStoryLine();
            movieDirectors = Arrays.toString(film.getDirectors());
            movieActors = Arrays.toString(film.getActors());

            movieDirectors = movieDirectors.replaceAll("\\[", "");
            movieDirectors = movieDirectors.replaceAll("]", "");
            movieActors = movieActors.replaceAll("\\[", "");
            movieActors = movieActors.replaceAll("]", "");
        }


        //finalise();

        setElementsAdditionalInfo();
        setElementsSize();
        setElementsLocation();

        infoLabel.updateUI();
        posterLabel.updateUI();
    }

    public void addActionListeners(InformationController listener) {
        streamButton.setActionCommand(ButtonCommandType.VIEW_FILM.id());
        streamButton.addActionListener(listener);
    }
}