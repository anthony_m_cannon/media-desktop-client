package com.mediaplus.views;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;

import javax.swing.JLabel;

import com.mediaplus.BaseObject;
import com.mediaplus.StartLoad;
import com.mediaplus.io.Resources;
import com.mediaplus.utils.Util;

public class SplashView extends View {
	private static final long serialVersionUID = 1L;
	
	private Image logoImage = Resources.IMAGE_LOGO;
	private JLabel loadingText;
	private int time;

	private float alpha;
    private int percentage;

    public SplashView(int width, int height) {
		super(width, height, false);

        time = 0;

		alpha = 0f;
		
		finalise();
	}
	
	@Override
	public void createElements() {
		loadingText = new JLabel("Loading");
	}

	@Override
	public void setElementsAdditionalInfo() {
		loadingText.setForeground(Color.WHITE);
		
		Font font = loadingText.getFont().deriveFont(25f);
		loadingText.setFont(font);
	}

	@Override
	public void setElementsSize() {
		loadingText.setSize(loadingText.getPreferredSize());
	}

	@Override
	public void setElementsLocation() {
		loadingText.setLocation(((windowWidth-loadingText.getWidth())/2)-6, (int) (windowHeight*0.85));
	}

	@Override
	public void addElements() {
		add(loadingText);
	}
	
	// Paint
	@Override
	public void paint(Graphics g) {
		super.paint(g);
        new Thread(new Runnable() {
            @Override
            public void run() {

            }
        }).start();
		Graphics2D g2d = (Graphics2D) g;

        if(logoImage != null) {
            int logoX = ((windowWidth / 2) - (logoImage.getWidth(this) / 2));
            int logoY = ((windowHeight / 2) - (logoImage.getHeight(this) / 2));

            if (alpha != 1.0f) {
                g2d.setComposite(AlphaComposite.getInstance(
                        AlphaComposite.SRC_OVER, alpha));
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

                changeAlphaValue();
            }
            if(percentage != 100) {
                changeLoadingValue();
            }
            g2d.drawImage(logoImage, logoX, logoY, null);
        }
        if(percentage != 100) {
            repaint();
        }
	}
	
	// Private
	private void changeAlphaValue() {
		alpha += 0.001f;

	    if (alpha >= 1.0f) {
	        alpha = 1.0f;
	    }
	}

	private void changeLoadingValue() {
        int max = 100;
        int current = 0;

        if(BaseObject.client != null){
            max = BaseObject.client.getNoFilms() + BaseObject.client.getNoMusic() + BaseObject.client.getNoImages();
            current = BaseObject.client.getLoadedMovies() + BaseObject.client.getLoadedMusic() + BaseObject.client.getLoadedImages();
        }
        
        percentage = 0;
        if(BaseObject.client != null){
            percentage = Util.getPercentage(max, current);
        }

        String status = "";
        switch (StartLoad.getLoadStatus()){
            case PREPARING:
                status = "Preparing to load...";
                break;
            case CONNECTING:
                status = "Connecting...";
                break;
            case FILM:
                status = "Fetching films...";
                break;
            case MUSIC:
                status = "Fetching music...";
                break;
            case IMAGES:
                status = "Fetching images...";
                break;
            case INITIALISING:
                status = "Initialising";
                break;
        }

        loadingText.setText("<html><body><center>Loading (" + percentage + "%)<br/><span style='font-size:10px'>" + status + "</span></center></body></html>");
        loadingText.setSize(loadingText.getPreferredSize());

        loadingText.setLocation(((windowWidth - loadingText.getWidth()) / 2) - 6, (int) (windowHeight * 0.85));

        if (time == 200) {
			time = 0;
		} else {
			time++;
		}
	}
}
