package com.mediaplus.views;

import java.awt.event.ActionListener;

import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;

import com.mediaplus.utils.PlayerBar;

public class PlayerView extends View {
	private static final long serialVersionUID = 1L;

	private EmbeddedMediaPlayerComponent playerComponent;
	private PlayerBar userBar;
	private int pin;
	
    public PlayerView(int width, int height, int pin) {
		super(width, height, false);
		
		this.pin = pin;
    }
    
    @Override
    public void finalise() {
    	super.finalise();
    	
    	start();
    }
	
	public void setMediaCompontent(EmbeddedMediaPlayerComponent playerComponent) {
		this.playerComponent = playerComponent;
    }

	/**
	 * Create canvas element and image for rendering. Starts rendering thread
	 * once everything's loaded.
	 */
	@Override
	public void createElements() {
        setLayout(null);
        
        userBar = new PlayerBar(windowWidth, (int) (windowHeight * 0.1), pin);
        userBar.createView();
    }

	@Override
	public void setElementsAdditionalInfo() {
		
	}

	@Override
	public void setElementsSize() {
        if(playerComponent != null) {
            this.playerComponent.setSize(windowWidth, (int) (windowHeight - (windowHeight * 0.1)));
        }
        userBar.setSize(windowWidth, (int) (windowHeight * 0.1));
	}

	@Override
	public void setElementsLocation() {
		playerComponent.setLocation(0, 0);
        userBar.setLocation(0, playerComponent.getHeight());
	}

	@Override
	public void addElements() {
        add(playerComponent);
        add(userBar);
	}
	
	public void addActionListener(ActionListener listener) {
		userBar.addActionListener(listener);
	}
	
	public void start() {
		userBar.start();
	}
}