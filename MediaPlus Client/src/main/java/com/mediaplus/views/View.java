package com.mediaplus.views;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.mediaplus.BaseObject;
import com.mediaplus.controllers.ConfigController;
import com.mediaplus.controllers.HomeController;
import com.mediaplus.interfaces.ViewInterface;
import com.mediaplus.io.Resources;
import com.mediaplus.types.ControllerType;

/**
 * Created by Deividas on 14/11/2014.
 */
public abstract class View extends Container implements ViewInterface, MouseListener {
	private static final long serialVersionUID = 1L;

	public static int windowWidth, windowHeight;
    public JPanel header;

    public View(int width, int height) {
        this(width, height, true);
    }

    public View(int width, int height, boolean showHeader){
        windowWidth = width;
        windowHeight = height;
        if(showHeader) {
            header = new JPanel();
            header.setLayout(null);
            header.setBackground(Color.BLACK);
            header.setPreferredSize(new Dimension(windowWidth, Resources.IMAGE_LOGO.getHeight(null)));

            JLabel logo = new JLabel();
            logo.setIcon(Resources.imageToIcon(Resources.IMAGE_LOGO));
            logo.setSize(logo.getPreferredSize());
            logo.setLocation((windowWidth/2) - (logo.getWidth()/2), 0);

            JButton back = new JButton();
            setButtonImage(back, Resources.IMAGE_BUTTON_BACK, Resources.IMAGE_BUTTON_BACK_HOVER);
            back.setLocation(5, 5);
            back.setSize(back.getPreferredSize());
            back.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                	if (!(BaseObject.bootstrap.getCurrentController() instanceof HomeController)) {
                		BaseObject.bootstrap.loadController(BaseObject.bootstrap.getController(ControllerType.HOME));
                	}
                }
            });

            JButton config = new JButton();
            setButtonImage(config, Resources.IMAGE_BUTTON_SETTINGS, Resources.IMAGE_BUTTON_SETTINGS_HOVER);
            config.setLocation(windowWidth - Resources.IMAGE_BUTTON_SETTINGS.getWidth(null) - 10, 5);
            config.setSize(config.getPreferredSize());
            config.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                	if (!(BaseObject.bootstrap.getCurrentController() instanceof ConfigController)) {
                		BaseObject.bootstrap.loadController(BaseObject.bootstrap.getController(ControllerType.CONFIG));
                	}
                }
            });

            header.add(logo);
            header.add(back);
            header.add(config);
            windowHeight = windowHeight - Resources.IMAGE_LOGO.getHeight(null);
        }
    }

	public void finalise() {
		createView();
	}

	public void createView() {
		createElements();
		setElementsAdditionalInfo();
		setElementsSize();
		setElementsLocation();
		addElements();
    }

	@Override
	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
        g2d.setPaint(Color.black);
        g2d.fillRect(0, 0, windowWidth, windowHeight);

		super.paint(g); // Paint elements
	}

	public abstract void createElements();

	public abstract void setElementsAdditionalInfo();

	public abstract void setElementsSize();

	public abstract void setElementsLocation();

	public abstract void addElements();

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    protected void setButtonImage(JButton button, Image icon, Image iconHover){
        button.setIcon(new ImageIcon(icon));
        button.setRolloverIcon(new ImageIcon(iconHover));
        button.setContentAreaFilled(false);
        button.setBorder(BorderFactory.createEmptyBorder());
    }
}
