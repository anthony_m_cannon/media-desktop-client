package com.mediaplus.views;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;

import com.mediaplus.BaseObject;
import com.mediaplus.controllers.HomeController;
import com.mediaplus.io.Resources;
import com.mediaplus.types.HomeCategoryType;
import com.mediaplus.types.HomeLayoutType;
import com.mediaplus.utils.ButtonHandler;
import com.mediaplus.views.home.categories.FilmCategory;
import com.mediaplus.views.home.categories.HomeCategory;
import com.mediaplus.views.home.categories.ImageCategory;
import com.mediaplus.views.home.categories.MusicCategory;

/**
 * Created by Deividas on 14/11/2014.
 */
public class HomeView extends View {
    private static final long serialVersionUID = 1L;

    private ButtonHandler bh;
    private HomeCategory category;

    private HomeCategoryType hct;
    private HomeLayoutType hlt;

    private ArrayList<String> titles;
    private ArrayList<Image> covers;

    private int categoryWidth, categoryHeight;

    private JButton viewBtn;
    private JButton imageBtn, musicBtn, filmBtn;
    private JLabel pinLabel;
    private int categoryYOffset = 20;
    private int categoryXOffset = 20;

    public HomeView(HomeCategoryType hct, HomeLayoutType hlt,
                    ArrayList<String> titles, ArrayList<Image> covers,
                    int width, int height) {
        super(width, height);

        this.hct = hct;
        this.hlt = hlt;

        this.titles = titles;
        this.covers = covers;

        categoryWidth = width - 20;
        categoryHeight = height / 2 + 50;

        bh = new ButtonHandler();

        finalise();
    }

    @Override
    public void finalise() {
        super.finalise();
    }

    public void addActionListeners(HomeController homeController) {
        viewBtn.addActionListener(homeController);

        imageBtn.addActionListener(homeController);
        musicBtn.addActionListener(homeController);
        filmBtn.addActionListener(homeController);
    }

    @Override
    public void createElements() {

        switch (hct) {
            case IMAGES:
                category = new ImageCategory(hlt, titles, covers, categoryWidth, categoryHeight);
                break;
            case MUSIC:
                category = new MusicCategory(hlt, titles, covers, categoryWidth, categoryHeight);
                break;
            case FILMS:
                category = new FilmCategory(hlt, titles, covers, categoryWidth, categoryHeight);
                break;
        }

        viewBtn = bh.getViewBtn();

        imageBtn = bh.getImageBtn();
        musicBtn = bh.getMusicBtn();
        filmBtn = bh.getFilmBtn();
        if(BaseObject.client != null) {
            pinLabel = new JLabel("Your Pin: " + BaseObject.client.getDesktopPin());
        }else{
            pinLabel = new JLabel("Your Pin: N/A");
        }
    }

    @Override
    public void setElementsAdditionalInfo() {
        pinLabel.setForeground(Color.WHITE);
        Font temp = pinLabel.getFont();
        Font font = new Font(temp.getFamily(), Font.PLAIN, 18);
        pinLabel.setFont(font);

        setButtonImage(imageBtn, Resources.IMAGE_BUTTON_IMAGES, Resources.IMAGE_BUTTON_IMAGES_HOVER);
        setButtonImage(musicBtn, Resources.IMAGE_BUTTON_MUSIC, Resources.IMAGE_BUTTON_MUSIC_HOVER);
        setButtonImage(filmBtn, Resources.IMAGE_BUTTON_VIDEOS, Resources.IMAGE_BUTTON_VIDEOS_HOVER);
    }

    @Override
    public void setElementsSize() {
        category.setSize(windowWidth - (categoryXOffset * 2), categoryHeight);
        viewBtn.setSize(32, 32);

        imageBtn.setSize(imageBtn.getPreferredSize());
        musicBtn.setSize(musicBtn.getPreferredSize());
        filmBtn.setSize(filmBtn.getPreferredSize());
        pinLabel.setSize(pinLabel.getPreferredSize());
    }

    @Override
    public void setElementsLocation() {
        category.setLocation(categoryXOffset, imageBtn.getHeight() + categoryYOffset);
        viewBtn.setLocation(windowWidth - viewBtn.getWidth() - 12, windowHeight - viewBtn.getHeight() - 35);

        int mediaButtonY =  category.getY() - imageBtn.getHeight();

        musicBtn.setLocation((windowWidth/2) - (musicBtn.getWidth()/2), mediaButtonY - 15);
        imageBtn.setLocation(musicBtn.getX() - imageBtn.getWidth() - 10, mediaButtonY- 15);
        filmBtn.setLocation(imageBtn.getX() + imageBtn.getWidth() + filmBtn.getWidth() + 20, mediaButtonY- 15);

        pinLabel.setLocation(20, (int) windowHeight - pinLabel.getHeight() - 40);
    }

    @Override
    public void addElements() {
        add(category);
        add(viewBtn);

        add(imageBtn);
        add(musicBtn);
        add(filmBtn);
        add(pinLabel);
    }

}