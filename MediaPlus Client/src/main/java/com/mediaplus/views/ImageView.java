package com.mediaplus.views;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferStrategy;

import com.mediaplus.controllers.ImageController;

/**
 * Created by Deividas on 26/03/2015.
 */
public class ImageView extends View implements Runnable {
	private static final long serialVersionUID = 1L;
	
	private Canvas imageCanvas;
    private Image image;
    private Image scaled;
    private int imageTopBotMargins = 20;

    public ImageView(int width, int height, Image img) {
        super(width, height);
        this.image = img;
        finalise();

        Thread t = new Thread(this);
        t.start();
    }

    @Override
    public void createElements() {
        setLayout(null);
        imageCanvas = new Canvas();
    }

    @Override
    public void setElementsAdditionalInfo() {
    }

    @Override
    public void setElementsSize() {
        if(image != null) {
            int imageWidth = image.getWidth(null);
            int imageHeight = image.getHeight(null);

            if (image.getWidth(null) > windowWidth) {
                imageWidth = windowWidth;
            }
            if (image.getHeight(null) > windowHeight) {
                imageHeight = windowHeight - imageTopBotMargins;
            }
            Dimension imgSize = new Dimension(image.getWidth(null), image.getHeight(null));
            Dimension panelSize = new Dimension(imageWidth, imageHeight);
            Dimension scaleSize = getScaledDimension(imgSize, panelSize);
            scaled = image.getScaledInstance((int) scaleSize.getWidth(), (int) scaleSize.getHeight(), Image.SCALE_SMOOTH);

            imageCanvas.setSize(scaled.getWidth(null), scaled.getHeight(null));
        }else{
            imageCanvas.setSize(windowWidth, windowHeight-50);
        }
    }

    @Override
    public void setElementsLocation() {
        imageCanvas.setLocation((windowWidth / 2) - (image.getWidth(null)/2), imageTopBotMargins);
//        imageCanvas.setLocation(scaled, 0);
    }

    @Override
    public void addElements() {
        setBackground(Color.BLACK);
        add(imageCanvas);
        render();
    }

    private void render() {
        if(isValid() && isValid()) {
            BufferStrategy bs = imageCanvas.getBufferStrategy();
            if (bs == null) {
                imageCanvas.createBufferStrategy(2);
                return;
            }
            Graphics g = bs.getDrawGraphics();

            if(scaled != null){
                g.drawImage(scaled, 0, 0, null);
            }

//            if(image != null) {
//                Dimension imgSize = new Dimension(image.getWidth(null), image.getHeight(null));
//                Dimension panelSize = new Dimension(windowWidth, windowHeight - (logoLabel.getY() + logoLabel.getHeight() + 40) - 40);
//                Dimension scaleSize = getScaledDimension(imgSize, panelSize);
//                Image scaled = image.getScaledInstance((int) scaleSize.getWidth(), (int) scaleSize.getHeight(), Image.SCALE_SMOOTH);
//                g.drawImage(scaled, (windowWidth/2) - (scaled.getWidth(null)/2), logoLabel.getY() + logoLabel.getHeight() + 40, null);
//            }
            bs.show();
            g.dispose();
        }
    }

    public void addActionListeners(ImageController controller) {
    }

    @Override
    public void run() {
        while(true){
            render();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {}
        }
    }
//
//    private class ImagePanel extends JPanel{
//
//
//        private final Image img;
//
//        public ImagePanel(Image image){
//            this.img = image;
//        }
//
//        @Override
//        protected void paintComponent(Graphics g) {
//            super.paintComponent(g);
//            if(img != null) {
//                Dimension imgSize = new Dimension(img.getWidth(null), img.getHeight(null));
//                Dimension panelSize = new Dimension(getWidth(), getHeight());
//                Dimension scaleSize = getScaledDimension(imgSize, panelSize);
//                g.setColor(Color.BLACK);
//                g.fillRect(logoLabel.getX(), logoLabel.getY(), windowWidth, windowHeight);
//                Image scaled = img.getScaledInstance((int) scaleSize.getWidth(), (int) scaleSize.getHeight(), Image.SCALE_SMOOTH);
//                g.drawImage(scaled, getX(), getY(), null);
//            }
//        }
//    }
//

    //SOURCE: http://stackoverflow.com/questions/10245220/java-image-resize-maintain-aspect-ratio
    public static Dimension getScaledDimension(Dimension imgSize, Dimension boundary) {

        int original_width = imgSize.width;
        int original_height = imgSize.height;
        int bound_width = boundary.width;
        int bound_height = boundary.height;
        int new_width = original_width;
        int new_height = original_height;

        // first check if we need to scale width
        if (original_width > bound_width) {
            //scale width to fit
            new_width = bound_width;
            //scale height to maintain aspect ratio
            new_height = (new_width * original_height) / original_width;
        }

        // then check if we need to scale even with the new height
        if (new_height > bound_height) {
            //scale height to fit instead
            new_height = bound_height;
            //scale width to maintain aspect ratio
            new_width = (new_height * original_width) / original_height;
        }

        return new Dimension(new_width, new_height);
    }
}
