package com.mediaplus.views.home.adapters;

import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;

public class HomeListAdapter extends DefaultListCellRenderer {
	private static final long serialVersionUID = 1L;

	private Font font;
	private ArrayList<Image> covers;

	public HomeListAdapter(ArrayList<Image> covers) {
		this.covers = covers;
		font = new Font("helvitica", Font.BOLD, 18);
	}

	@Override // JList is '<?>' as it's only used to pass to super
	public Component getListCellRendererComponent(JList<?> list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {

		ImageIcon image = ImageToIcon(covers.get(index));
		JLabel label = (JLabel) super.getListCellRendererComponent(list, value,
				index, isSelected, cellHasFocus);
        label.setForeground(new Color(215, 215, 215));

        if (isSelected) {
            label.setBackground(new Color(20, 20, 20));
            label.setBorder(BorderFactory.createEmptyBorder());
        }

        label.setIcon(image);
		label.setHorizontalTextPosition(JLabel.RIGHT);
		label.setFont(font);

		return label;
	}

	private ImageIcon ImageToIcon(Image image) {
		image = image.getScaledInstance(15, 30, 0);

		ImageIcon icon = new ImageIcon(image);

		return icon;
	}
}
