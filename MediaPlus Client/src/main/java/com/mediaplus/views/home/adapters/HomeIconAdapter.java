package com.mediaplus.views.home.adapters;

import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;

public class HomeIconAdapter extends DefaultListCellRenderer {
	private static final long serialVersionUID = 1L;

	private Font font;
	private ArrayList<Image> covers;
	private int width;

	public HomeIconAdapter(ArrayList<Image> covers) {
		this.covers = covers;
		font = new Font("helvitica", Font.BOLD, 8);
		width = 60;
	}

	@Override // JList is '<?>' as it's only used to pass to super
	public Component getListCellRendererComponent(JList<?> list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {

		String text =  "<html><body style='width:" + width + "px; text-align: center;'>" + value.toString() + "</html>";
		ImageIcon image = ImageToIcon(covers.get(index));
		
		JLabel label = (JLabel) super.getListCellRendererComponent(list, text,
				index, isSelected, cellHasFocus);

        label.setForeground(new Color(215, 215, 215));

        if (isSelected) {
            label.setBackground(new Color(20, 20, 20));
            label.setBorder(BorderFactory.createEmptyBorder());
        }

		label.setIcon(image);
		label.setSize(width, 120);
		
		label.setHorizontalAlignment(JLabel.CENTER);
		label.setVerticalAlignment(JLabel.TOP);
		
		label.setHorizontalTextPosition(JLabel.CENTER);
		label.setVerticalTextPosition(JLabel.BOTTOM);
		
		label.setFont(font);

		return label;
	}

	private ImageIcon ImageToIcon(Image image) {
		image = image.getScaledInstance(width, 100, 0);

		ImageIcon icon = new ImageIcon(image);

		return icon;
	}
}