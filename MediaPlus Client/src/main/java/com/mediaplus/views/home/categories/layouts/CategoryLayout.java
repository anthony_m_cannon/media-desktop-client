package com.mediaplus.views.home.categories.layouts;

import java.awt.Image;
import java.util.ArrayList;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.mediaplus.interfaces.ViewInterface;
import com.mediaplus.utils.HomeItemListener;

public abstract class CategoryLayout extends JPanel implements ViewInterface {
	private static final long serialVersionUID = 1L;

	private ArrayList<String> titles;
	private ArrayList<Image> covers;
	public static JList<String> list;
	public static JScrollPane scroll;

	public CategoryLayout(ArrayList<String> titles, ArrayList<Image> covers) {
		
		this.titles = titles;
		this.covers = covers;
		
	}
	
	public void finalise() {
		createView();
	}

	public void createView() {
		createElements();
		setElementsAdditionalInfo();
		setElementsSize();
		setElementsLocation();
		addElements();
		addListener();
	}
	
	private void addListener() {
        list.addMouseListener(new HomeItemListener());
	}

	// Gets
	public ArrayList<String> titles() {
		return titles;
	}
	
	public ArrayList<Image> covers() {
		return covers;
	}
	
	public JList<String> getList() {
		return list;
	}
	
	public JScrollPane getScroll() {
		return scroll;
	}
	
	// Abstracts
	public abstract void createElements();
	public abstract void setElementsAdditionalInfo();
	public abstract void setElementsSize();
	public abstract void setElementsLocation();
	public abstract void addElements();
}
