package com.mediaplus.views.home.categories;

import java.awt.Image;
import java.util.ArrayList;

import com.mediaplus.types.HomeLayoutType;

public class FilmCategory extends HomeCategory {
	private static final long serialVersionUID = 1L;
	
	public FilmCategory(HomeLayoutType hlt, ArrayList<String> titles, ArrayList<Image> covers, int categoryWidth, int categoryHeight) {
		super(hlt, titles, covers, categoryWidth, categoryHeight);
		
		finalise();
	}

}
