package com.mediaplus.views.home.categories.layouts;

import java.awt.*;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import com.mediaplus.views.home.adapters.HomeIconAdapter;

public class IconLayout extends CategoryLayout {
	private static final long serialVersionUID = 1L;

	public IconLayout(ArrayList<String> titles, ArrayList<Image> covers) {
		super(titles, covers);
		
		finalise();
	}

	@Override
	public void createElements() {
		ArrayList<String> t = titles();
		String[] titles = new String[t.size()];
		for (int i=0; i<titles.length; i++) {
			titles[i] = t.get(i);
		}
		
		list = new JList<String>(titles);
		scroll = new JScrollPane(list,
	            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
	            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	}

	@Override
	public void setElementsAdditionalInfo() {
		list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		list.setVisibleRowCount(3);
	    list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    
		list.setCellRenderer( new HomeIconAdapter(covers()) );

        list.setBackground(new Color(30, 30, 30));
        list.setBorder(BorderFactory.createEmptyBorder());
        scroll.setBackground(new Color(30, 30, 30));
        scroll.setBorder(BorderFactory.createEmptyBorder());

        int gap = 0;
        setLayout(new BorderLayout(gap, gap));
        setBorder(BorderFactory.createEmptyBorder(gap , gap, gap, gap));
	}

	@Override
	public void setElementsSize() {

	}

	@Override
	public void setElementsLocation() {
		
	}

	@Override
	public void addElements() {
		add(scroll, BorderLayout.CENTER);
	}
}
