package com.mediaplus.views.home.categories;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.JPanel;

import com.mediaplus.interfaces.ViewInterface;
import com.mediaplus.types.HomeLayoutType;
import com.mediaplus.views.home.categories.layouts.CategoryLayout;
import com.mediaplus.views.home.categories.layouts.IconLayout;
import com.mediaplus.views.home.categories.layouts.ListLayout;
import com.mediaplus.views.home.categories.layouts.TileLayout;

public abstract class HomeCategory extends JPanel implements ViewInterface {
	private static final long serialVersionUID = 1L;

	private HomeLayoutType hlt;
	private CategoryLayout categoryLayout;
	
	private ArrayList<String> titles;
	private ArrayList<Image> covers;
	
	private int width, height;
	
	public HomeCategory(HomeLayoutType hlt, ArrayList<String> titles, ArrayList<Image> covers, int categoryWidth, int categoryHeight) {
		super();
		
		this.hlt = hlt;
		
		this.titles = titles;
		this.covers = covers;
		
		this.width = categoryWidth;
		this.height = categoryHeight;
	}
	
	public void finalise() {
		createView();
		
		setLayout(null);
	}
	
	@Override
	public void createView() {
		createElements();
		setElementsAdditionalInfo();
		setElementsSize();
		setElementsLocation();
		addElements();
	}

	@Override
	public void createElements() {
        setBackground(new Color(30, 30, 30));
        switch(hlt) {
		case TILES:
			categoryLayout = new TileLayout(titles, covers);
			break;
		case LIST:
			categoryLayout = new ListLayout(titles, covers);
			break;
		case ICONS:
			categoryLayout = new IconLayout(titles, covers);
			break;
		}
    }

	@Override
	public void setElementsAdditionalInfo() {
        categoryLayout.setBackground(new Color(30, 30, 30));
	}

	@Override
	public void setElementsSize() {
		categoryLayout.setSize(width, height - 10);
	}

	@Override
	public void setElementsLocation() {
		categoryLayout.setLocation(10, 10);
	}

	@Override
	public void addElements() {
		add(categoryLayout, BorderLayout.SOUTH);
	}

}
