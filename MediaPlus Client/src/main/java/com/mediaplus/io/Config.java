package com.mediaplus.io;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.mediaplus.utils.ConfigDefaults;

public class Config {
	private final FileIO fileIo;
	private final ConfigDefaults defaults;

	public Config() {
		fileIo = new FileIO();
		defaults = new ConfigDefaults();
	}
	
	public ConfigDefaults getDefaults() {
		return defaults;
	}
	
	// gets
	public HashMap<String, String> get() {
		HashMap<String, String> config = new HashMap<String, String>();
		String[] lines;

		if (configExists()) {
			lines = fileIo.readFile("config.ini");
		} else {
			createNewConfig();
			populateConfig(defaults.toArray());
			lines = defaults.toArray();
		}

		for (String line : lines) {
			String[] value = line.split("=");
			config.put(value[0], value[1]);
		}

		return config;
	}
	
	// sets
	public boolean set(HashMap<String, String> config) {
		String[] sArray = hashMapToArrayList(config);

		return fileIo.writeToFile("config.ini", false, sArray);
	}

	// Private
	private String[] hashMapToArrayList(HashMap<String, String> hMap) {
		String[] array = new String[hMap.size()];
		int i = 0;
		
		for (Map.Entry<String, String> entry : hMap.entrySet()) {
			array[i] = entry.getKey() + "=" + entry.getValue();
			i++;
		}
		
		System.out.println("Converted to: " + Arrays.toString(array));
		return array;
	}

	private boolean configExists() {
		return fileIo.fileExists("config.ini");
	}

	private boolean createNewConfig() {
		return fileIo.createNewFile("config.ini");
	}

	private boolean populateConfig(String[] config) {

		return fileIo.writeToFile("config.ini", false, config);
	}
}
