package com.mediaplus.io;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;

import uk.co.caprica.vlcj.player.MediaPlayer;

import com.mediaplus.BaseObject;
import com.mediaplus.controllers.InformationController;
import com.mediaplus.controllers.PlayerController;
import com.mediaplus.io.packet.HandshakePacket;
import com.mediaplus.io.packet.Packet;
import com.mediaplus.io.packet.command.CommandPacket;
import com.mediaplus.io.packet.command.CommandProperty;
import com.mediaplus.io.packet.command.CommandType;
import com.mediaplus.media.Film;
import com.mediaplus.types.ControllerType;
import com.sun.org.apache.xml.internal.security.utils.Base64;

/**
 * Created by Deividas on 09/02/2015.
 */
public class Client implements Runnable {
	public static int noFilms;
	public static int filmInfo;

	private Socket socket;
	private InputStream in;
	private OutputStream out;

	private int desktopPin;
	private int noImages;
	private int noMusic;

	private boolean connected;

	private ArrayList<String> receivedFilms;
	private ArrayList<String> receivedInfoFilms;

	private int loadedMovies;
	private int loadedImages;
	private int loadedMusic;

	private String currentMrl;

	public Client() {
		com.sun.org.apache.xml.internal.security.Init.init();

		setDefaults();
	}

	private void setDefaults() {
		receivedFilms = new ArrayList<String>();
		receivedInfoFilms = new ArrayList<String>();
		loadedMovies = 0;
		loadedImages = 0;
		loadedMusic = 0;
		currentMrl = "";

		noFilms = -1;
		noImages = -1;
		noMusic = -1;
		filmInfo = 0;

		connected = false;

		socket = null;
		in = null;
		out = null;
	}

	public boolean isConnected() {
		return connected;
	}

	public boolean connect(String ip, int port) {
		try {
			System.out.println(ip + ":" + port);

			socket = new Socket(ip, port);
			in = socket.getInputStream();
			out = socket.getOutputStream();

			HandshakePacket handshake = new HandshakePacket();
			handshake.sendPacket(out);

			connected = true;

			Thread t = new Thread(this, "Update Thread");
			t.start();

			return true;
		} catch (IOException e) {
			return false;
		}
	}

	public boolean stop() {
		try {
			if (socket != null)
				socket.close();
			if (in != null)
				in.close();
			if (out != null)
				out.close();

			setDefaults();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public void run() {
		while (connected) {
			ArrayList<Packet> packets = Packet.readPacket(in, this);
			for (int i = 0; i < packets.size(); i++) {
				Packet p = packets.get(i);
				if (p == null) {
					connected = false;
				}

				if (p instanceof CommandPacket) {

					MediaPlayer player = BaseObject.player;
					CommandPacket commandPacket = (CommandPacket) p;

					CommandType commandType = CommandType
							.getCommandType(commandPacket.getCommandId());

					if (player == null
							&& commandType != CommandType.STREAM_FILM
							&& commandType != CommandType.GET_FILMS
							&& commandType != CommandType.GET_IMAGES
							&& commandType != CommandType.GET_MUSIC
							&& commandType != CommandType.INFO
							&& commandType != CommandType.SET_PROPERTY) {
						continue;
					}

					switch (commandType) {
					case GET_FILMS:
						String filmName = commandPacket.getCommandData();
						receivedFilms.add(filmName);

						String extension = filmName.substring(filmName
								.lastIndexOf("."));

						BaseObject.mediaHandler.newFilm(
								Resources.IMAGE_PICTURE_NOT_AVAILABLE,
								stripExtension(filmName), extension);

						CommandPacket command = new CommandPacket(
								CommandType.GET_PROPERTY.getId() + "0"
										+ CommandProperty.FILM_POSTER.getId()
										+ "" + filmName);
						command.sendPacket(out);
						break;
					case GET_MUSIC:
						String songName = commandPacket.getCommandData();
						BaseObject.mediaHandler.newSong(
								Resources.IMAGE_PICTURE_NOT_AVAILABLE,
								songName, "");
						loadedMusic++;
						break;
                        case GET_IMAGES:
                            String name = commandPacket.getCommandData().split("¦")[0];
                            String imgData = commandPacket.getCommandData().split(
                                    "¦")[1];
                            try {
                                byte[] decodedImageData = Base64.decode(imgData);
                                BufferedImage img = ImageIO
                                        .read(new ByteArrayInputStream(
                                                decodedImageData));
                                BaseObject.mediaHandler.newPicture(img, name);
                            } catch (Exception e) {
                                System.err.println(e.getMessage());
                            }
                            loadedImages++;

                            break;
					case INFO:

						String infoData = commandPacket.getCommandData();
						final String SEPERATOR = "¦";
						String[] data = infoData.split(SEPERATOR);

						System.out.println("DATA as STRING + "
								+ Arrays.toString(data));

						String title = "";
						int year = 0;
						int runTime = 0;
						String genre = "";
						double imdbRating = 0.0;
						double tomatoRating = 0.0;
						String[] directors = null;
						String[] actors = null;
						String plot = "";
						String poster = "";
						String extention = "";
						String rated = "";
						String country = "";
						String fileName = "";

						for (int j = 0; j < data.length; j++) {
							if (data[j].split("=").length < 2) {
								continue;
							}

							String infoKey = data[j].split("=")[0];
							String infoValue = data[j].split("=")[1];

							if (infoKey.equals("title")) {
								title = infoValue;
							} else if (infoKey.equals("year")) {

								year = Integer.parseInt(infoValue.replaceAll(
										"[^0-9]+", ""));
							} else if (infoKey.equals("runtime")) {
								String time = infoValue.split(" ")[0];
								runTime = Integer.parseInt(time);
							} else if (infoKey.equals("genre")) {
								genre = infoValue;
							} else if (infoKey.equals("runtime")) {
								String time = infoValue.split(" ")[0];
								runTime = Integer.parseInt(time);
							} else if (infoKey.equals("imdbRating")) {
								imdbRating = Double.parseDouble(infoValue);
							} else if (infoKey.equals("tomatoRating")) {
								if (infoValue.equals("N/A")) {
									tomatoRating = Double.parseDouble("0.0");
								} else {
									tomatoRating = Double
											.parseDouble(infoValue);
								}
							} else if (infoKey.equals("director")) {
								directors = infoValue.split(", ");
							} else if (infoKey.equals("actors")) {
								actors = infoValue.split(", ");
							} else if (infoKey.equals("plot")) {
								plot = infoValue;
							} else if (infoKey.equals("fileName")) {
								fileName = infoValue;
								extention = fileName.substring(fileName.lastIndexOf('.') + 1);
							} else if (infoKey.equals("rated")) {
								rated = infoValue;
							} else if (infoKey.equals("country")) {
								country = infoValue;
							}
						}

						Film film = BaseObject.mediaHandler
								.getFilmByFileName(fileName);
						film.setTitle(title);
						System.out.println("File name is : " + fileName
								+ " Title is: " + title);
						film.setYear(year);
						film.setRunTime(runTime);
						film.setGenre(genre);
						film.setCountry(country);
						film.setImdbRating(imdbRating);
						film.setTomatoRating(tomatoRating);
						film.setDirectors(directors);
						film.setActors(actors);
						film.setPlot(plot);

						filmInfo++;

						InformationController infoController = (InformationController) BaseObject.bootstrap
								.getController(ControllerType.INFORMATION);
						infoController.setFilm(fileName, true);
						BaseObject.bootstrap.loadController(infoController);

						break;
					case PLAY_PAUSE:
						if (player.isPlayable() && !player.isPlayable()) {
							player.play();
						} else {
							player.pause();
						}

						System.out.println(player.getTime());
						break;
					case STOP:
						player.stop();
						break;
					case MUTE:
						player.mute();
						break;
					case STREAM_FILM:
						PlayerController playerController;
						if (!(BaseObject.bootstrap.getCurrentController() instanceof PlayerController)) {
							playerController = (PlayerController) BaseObject.bootstrap
									.getController(ControllerType.PLAYER);
							BaseObject.bootstrap
									.loadController(playerController);
						} else {
							playerController = (PlayerController) BaseObject.bootstrap
									.getCurrentController();
						}
						System.out.println(commandPacket.getCommandData()
								+ " ---- --- ");
						String[] mrlData = commandPacket.getCommandData()
								.split("¦");
						currentMrl = mrlData[0];
						MediaPlayerListener.totalTime = Long
								.parseLong(mrlData[1]);

						playerController.loadVideo(currentMrl);

						loadVideoInfo();
						break;
					case PLAY_SONG:
						PlayerController musicPlayerController = (PlayerController) BaseObject.bootstrap
								.getController(ControllerType.PLAYER);
						BaseObject.bootstrap
								.loadController(musicPlayerController);
						musicPlayerController.loadVideo(commandPacket
								.getCommandData());
						System.out.println("Starting music stream");
						break;
					case SHOW_IMAGE:
						System.out.println("TODO: DISPLAY "
								+ commandPacket.getCommandData() + " IMAGE");
						break;
					case SET_PROPERTY:
						// int propertyId =
						// Integer.parseInt(commandPacket.getCommandData().substring(0,
						// 2));
						int propertyId = Integer.parseInt(commandPacket
								.getCommandData().substring(0, 2));

						String propertyData = commandPacket.getCommandData()
								.substring(2);
						CommandProperty property = CommandProperty
								.getProperty(propertyId);

						switch (property) {
						case ERROR:
							System.err
									.println("ERROR: Could not find 'SET_PROPERTY' property.");
							break;
						case VOLUME:
							if (BaseObject.player != null) {
								BaseObject.player.setVolume(Integer
										.parseInt(propertyData));
							}
							break;
						case NO_FILMS:
							noFilms = Integer.parseInt(propertyData);
							System.out.println("number of films: " + noFilms);
							break;
						case NO_IMAGES:
							noImages = Integer.parseInt(propertyData);
							System.out.println("number of images: " + noImages);
							break;
						case NO_MUSIC:
							noMusic = Integer.parseInt(propertyData);
							System.out.println("number of music: " + noMusic);
							break;
						case FILM_POSTER:
							String posterData[] = propertyData.split("¦");
							String movie = posterData[0];
							String moviePoster = posterData[1];
							BaseObject.mediaHandler.getFilmByFileName(movie)
									.setPoster(moviePoster);
							BaseObject.mediaHandler.getFilmByFileName(movie)
									.setTitle(stripExtension(movie));
							loadedMovies++;
							break;
						case CURRENT_PLAY_TIME:
							// playerController.loadVideo(currentMrl);
							// BaseObject.player.setTime(Long.parseLong(propertyData));
							break;
						default:
							System.out.println("UNKNOWN PROPERTY " + property
									+ " " + propertyId);
							break;
						}

						break;
					case GET_PROPERTY:
						int getPropId = Integer
								.parseInt(commandPacket.getCommandData()
										.substring(
												0,
												commandPacket.getCommandData()
														.length()));
						CommandProperty getProp = CommandProperty
								.getProperty(getPropId);
						switch (getProp) {
						default: // error
						case ERROR:
							break;
						case VOLUME:
							int volume = player.getVolume();
							if (volume == -1) {
								continue;
							}
							// make sure 3 numbers come back
							String vol = volume + "";
							for (int j = vol.length(); j < 3; j++) {
								vol = "0" + vol;
							}

							CommandPacket volCommand = new CommandPacket(
									CommandType.SET_PROPERTY.getId() + "0"
											+ CommandProperty.VOLUME.getId()
											+ vol);
							volCommand.sendPacket(out);
							break;
						case PAUSE_STATE:
							CommandPacket playPauseCommand = new CommandPacket(
									CommandType.SET_PROPERTY.getId()
											+ "0"
											+ CommandProperty.PAUSE_STATE
													.getId()
											+ (player.isPlaying() ? 1 : 0));
							playPauseCommand.sendPacket(out);

							break;
						case CURRENT_PLAY_TIME:
							CommandPacket currentTime = new CommandPacket(
									CommandType.SET_PROPERTY.getId()
											+ "0"
											+ CommandProperty.CURRENT_PLAY_TIME
													.getId()
											+ MediaPlayerListener.totalTime);
							currentTime.sendPacket(out);
							break;

						}
						break;
					default:
						System.out.println("Unknown command type. "
								+ commandType);
						break;
					}
				} else if (p instanceof HandshakePacket) {
					HandshakePacket handshakePacket = (HandshakePacket) p;
					desktopPin = handshakePacket.getPin();
					System.out.println("Your pin: " + desktopPin);

					// get films list
					CommandPacket getFilms = new CommandPacket(
							CommandType.GET_FILMS.getId() + "");
					getFilms.sendPacket(out);
				}
			}
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
			}
		}

		System.out.println("NO LONGER CONNECTED");
	}

	public String stripExtension(String fileName) {
		int index = fileName.lastIndexOf(".");

		if (index > 0) {
			fileName = fileName.substring(0, index);
		}

		return fileName;
	}

	private void loadVideoInfo() {
		sendRequest(CommandProperty.PLAY_TIME);
	}

	public void sendRequest(CommandProperty property) {
		String prop = "";
		if (property.getId() < 10) {
			prop += 0;
		}
		prop += property.getId();
		CommandPacket command = new CommandPacket(
				CommandType.GET_PROPERTY.getId() + "" + prop);
		command.sendPacket(out);
	}

	public OutputStream getOutputStream() {
		return out;
	}

	public int getNoFilms() {
		return noFilms;
	}

	public int getLoadedMovies() {
		return loadedMovies;
	}

	public ArrayList<String> getReceivedFilms() {
		return receivedFilms;
	}

	public ArrayList<String> getReceivedInfoFilms() {
		return receivedInfoFilms;
	}

	public int getDesktopPin() {
		return desktopPin;
	}

	public int getNoImages() {
		return noImages;
	}

	public int getNoMusic() {
		return noMusic;
	}

	public int getLoadedMusic() {
		return loadedMusic;
	}

	public int getLoadedImages() {
		return loadedImages;
	}
}
