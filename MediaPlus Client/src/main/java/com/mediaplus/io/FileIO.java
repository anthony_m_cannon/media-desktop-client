package com.mediaplus.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

public class FileIO {

	public FileIO() {

	}

	public String[] readFile(String fileUrl) {

		try {
			Path path = Paths.get(fileUrl);

			Object[] objectArray = Files.readAllLines(path,
					StandardCharsets.UTF_8).toArray();
			String[] fileLines = Arrays.copyOf(objectArray, objectArray.length,
					String[].class);

			return fileLines;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new String[0]; // return a blank string array
	}

	public String[] listFilesInFolder(String url) {
		String[] contents = listContentsInFolder(url);
		ArrayList<String> files = new ArrayList<String>();

		for (String fileUrl : contents) {
			File file = new File(fileUrl);

			if (!file.isDirectory()) {
				files.add(file.getName());
			}
		}

		String[] fileNames = ArrayListToStringArray(files);

		return fileNames;
	}

	public boolean fileExists(String fileUrl) {
		File f = new File(fileUrl);

		if (f.exists() && !f.isDirectory()) {
			return true;
		}

		return false;
	}

	public boolean folderExists(String folderUrl) {
		File f = new File(folderUrl);

		if (f.exists() && f.isDirectory()) {
			return true;
		}

		return false;
	}

	public boolean createNewFile(String fileUrl) {
		try {
			File file = new File(fileUrl);
			return file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean createNewFolder(String folderUrl) {
		File folder = new File(folderUrl);

		return folder.mkdirs();
	}

	public boolean writeToFile(String fileUrl, boolean append, String line) {
		String[] lines = new String[] {line};
		return writeToFile(fileUrl, append, lines);
	}

	public boolean writeToFile(String fileUrl, boolean append, String[] lines) {
		try {
			FileWriter fw = new FileWriter(fileUrl, append);
			for (String line : lines) {
				System.out.println("Writing [" + line + "] to file [" + fileUrl + "]");
				fw.write(line + System.lineSeparator());
			}

			fw.flush();
			fw.close();

			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	// Private
	private String[] listContentsInFolder(String url) {
		File file = new File(url);
		File folder = new File(file.getAbsolutePath());

		File[] files = folder.listFiles();
		String[] fileNames = new String[files.length];

		for (int i = 0; i < files.length; i++) {
			fileNames[i] = files[i].getName();
		}

		return fileNames;
	}

	private String[] ArrayListToStringArray(ArrayList<String> arrayList) {
		String[] stringArray = new String[arrayList.size()];

		stringArray = arrayList.toArray(stringArray);

		return stringArray;

	}
}
