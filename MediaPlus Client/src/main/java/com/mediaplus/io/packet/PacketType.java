package com.mediaplus.io.packet;

public enum PacketType {
	ERROR(0x0), HANDSHAKE(0x1), COMMAND(0x2);

	private int id;

	PacketType(int id) {
		this.id = id;
	}

	public static PacketType getPacketType(int id) {
		switch (id) {
		default:
			return ERROR;
		case 0x0:
			return ERROR;
		case 0x1:
			return HANDSHAKE;
		case 0x2:
			return COMMAND;
		}
	}

	public int id() {
		return id;
	}
}