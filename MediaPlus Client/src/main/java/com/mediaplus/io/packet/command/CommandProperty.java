package com.mediaplus.io.packet.command;

/**
 * Created by Deividas on 15/03/2015.
 */
public enum CommandProperty {

    ERROR(0), VOLUME(1), PLAY_TIME(2), VIDEO_CHANNEL(3), AUDIO_CHANNEL(4), SUBTITLE_CHANNEL(5), PAUSE_STATE(6), CURRENT_PLAY_TIME(7),
    NO_FILMS(8), FILM_POSTER(9), NO_IMAGES(10), NO_MUSIC(11);

    private int id;

    CommandProperty(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static CommandProperty getProperty(int id) {
        switch (id) {
            case 0:
                return ERROR;
            case 1:
                return VOLUME;
            case 2:
                return PLAY_TIME;
            case 3:
                return VIDEO_CHANNEL;
            case 4:
                return AUDIO_CHANNEL;
            case 5:
                return SUBTITLE_CHANNEL;
            case 6:
                return PAUSE_STATE;
            case 7:
                return CURRENT_PLAY_TIME;
            case 8:
                return NO_FILMS;
            case 9:
                return FILM_POSTER;
            case 10:
                return NO_IMAGES;
            case 11:
                return NO_MUSIC;
        }
        return null;
    }
}