package com.mediaplus.io.packet;

import com.mediaplus.io.Client;
import com.mediaplus.io.packet.command.CommandPacket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.ArrayList;

/**
 * Created by Deividas on 09/02/2015.
 */
public class Packet {
	protected int id;
	protected String data;

	public int getId() {
		return id;
	}

	public Packet(int packetId, String data) {
		this.id = packetId;
		this.data = data;
	}

    public static final String END_OF_INPUT = "0END0";

    public byte[] buildPacket(){
        String packetString = "";
        if(id < 10){
            packetString += "0";
        }
        packetString += id;
        packetString += data;
        packetString += END_OF_INPUT;

        return packetString.getBytes();
    }

	public boolean sendPacket(OutputStream out) {
		try {
			out.write(buildPacket());
			out.flush();
            return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

//	public static Packet readPacket(InputStream in, Client client) {
//		try {
//			// allocate byte buffer
//			byte[] packetBytes = new byte[1024];
//			while (in.read(packetBytes) != -1) {
//				String packetStr = new String(packetBytes, "UTF-8");
//				String packetIdStr = packetStr.substring(0, 2);
//
//				int packetId = Integer.parseInt(packetIdStr);
//				PacketType packetType = PacketType.getButtonType(packetId);
//				String packetData = "";
//
//				if (packetStr.length() > 2) {
//					packetData = packetStr.substring(2);
//				}
//
//				packetData = packetData.trim();
//				switch (packetType) {
//				default:
//
//					break;
//				case HANDSHAKE:
//					int pin = Integer.parseInt(packetData);
//					return new HandshakePacket(pin);
//				case COMMAND:
//					return new CommandPacket(packetData);
//				}
//			}
//		} catch (SocketException sockE) {
//			if (sockE.getMessage().equalsIgnoreCase("connection reset")) {
//				client.disconnect();
//
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//		return null;
//	}

    public static ArrayList<Packet> readPacket(InputStream in, Client client) {
        ArrayList<Packet> packetBuffer = new ArrayList<Packet>();
        try{
            byte[] packetBytes = new byte[Character.MAX_VALUE];
            try {
                while (in.read(packetBytes, 0, in.available()) != -1) {
                    String packetStr = new String(packetBytes, "UTF-8");
                    String[] subPackets = packetStr.split(String.valueOf(END_OF_INPUT));
                    for (int i = 0; i < subPackets.length; i++) {
                        String subPacketStr = subPackets[i];

                        String packetIdStr = subPacketStr.substring(0, 2);
                        if (packetIdStr.equals("  ")) continue;

                        int packetId = Integer.parseInt(packetIdStr);
                        PacketType packetType = PacketType.getPacketType(packetId);
                        String packetData = "";
                        if (packetStr.length() > 2) {
                            packetData = subPacketStr.substring(2);
                        }
                        packetData = packetData.trim();
                        packetData.replaceAll((char) 65535 + "", "");
                        switch (packetType) {
                            default:

                                break;
                            case HANDSHAKE:
                                int pin = Integer.parseInt(packetData);
                                packetBuffer.add(new HandshakePacket(pin));
                                break;
                            case COMMAND:
                                packetBuffer.add(new CommandPacket(packetData));
                                break;
                        }
                    }
                    return packetBuffer;
                }
            }catch(ArrayIndexOutOfBoundsException e){
                return packetBuffer;
            }

        }catch(SocketException sockE){
            if(sockE.getMessage().equalsIgnoreCase("connection reset")){
                client.stop();

            }
        }catch(IOException e){
            e.printStackTrace();
        }catch(java.lang.NumberFormatException e){
        	
        }catch(Exception e){
        	e.printStackTrace();
        }
        return packetBuffer;
    }

}
