package com.mediaplus.io.packet.command;

/**
 * Created by Deividas on 06/03/2015.
 */
public enum CommandType { // make property type (enum) - for get and set property
    ERROR(0x0), PLAY_PAUSE(0x1), STOP(0x2), LEFT(0x3), RIGHT(0x4), UP(0x5), DOWN(
            0x6), ENTER(0x7), BACK(0x8), INFO(0x9), REPEAT(10), SHUFFLE(11), GET_PROPERTY(
            12), SET_PROPERTY(13), MUTE(14), POWER(15), STREAM_FILM(16), GET_FILMS(
            17), BE_REMOTE(18), GET_IMAGES(19), GET_MUSIC(20), PLAY_SONG(21), SHOW_IMAGE(22);

    private int id;

    CommandType(int id) {
        this.id = id;
    }

    public static CommandType getCommandType(int id) {
        switch (id) {
            default:
                return ERROR;
            case 0:
                return ERROR;
            case 1:
                return PLAY_PAUSE;
            case 2:
                return STOP;
            case 3:
                return LEFT;
            case 4:
                return RIGHT;
            case 5:
                return UP;
            case 6:
                return DOWN;
            case 7:
                return ENTER;
            case 8:
                return BACK;
            case 9:
                return INFO;
            case 10:
                return REPEAT;
            case 11:
                return SHUFFLE;
            case 12:
                return GET_PROPERTY;
            case 13:
                return SET_PROPERTY;
            case 14:
                return MUTE;
            case 15:
                return POWER;
            case 16:
                return STREAM_FILM;
            case 17:
                return GET_FILMS;
            case 18:
                return BE_REMOTE;
            case 19:
            	return GET_IMAGES;
            case 20:
            	return GET_MUSIC;
            case 21:
                return PLAY_SONG;
            case 22:
                return SHOW_IMAGE;
        }
    }

    public int getId() {
        return id;
    }
}