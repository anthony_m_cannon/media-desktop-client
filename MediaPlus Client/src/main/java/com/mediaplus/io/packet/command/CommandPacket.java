package com.mediaplus.io.packet.command;

import com.mediaplus.io.packet.Packet;

/**
 * Created by Deividas on 04/03/2015.
 */
public class CommandPacket extends Packet {

    private int controlCommand;
    private String controlParameter;

    public CommandPacket(String packetData) {
        super(2, packetData);

        if(packetData.length() > 2) {
            controlCommand = Integer.parseInt(packetData.substring(0, 2));
            controlParameter = packetData.substring(2, packetData.length());
        }else{
            controlCommand = Integer.parseInt(packetData.substring(0, packetData.length()));
        }
    }

    public int getCommandId() {
        return controlCommand;
    }

    public String getCommandData() {
        return controlParameter;
    }
}

