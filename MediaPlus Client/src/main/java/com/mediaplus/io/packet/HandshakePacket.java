package com.mediaplus.io.packet;


/**
 * Created by Deividas on 09/02/2015.
 */
public class HandshakePacket extends Packet{

    private static final int DESKTOP_HANDSHAKE = 0x1;
    private int pin;

    public HandshakePacket() {
        super(PacketType.HANDSHAKE.id(), "0" + DESKTOP_HANDSHAKE);
    }

    public HandshakePacket(int pin){
        super(PacketType.HANDSHAKE.id(), pin + "");
        this.pin = pin;
    }

    public int getPin(){
        return pin;
    }

}
