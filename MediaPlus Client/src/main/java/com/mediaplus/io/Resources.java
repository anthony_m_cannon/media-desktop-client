package com.mediaplus.io;

import java.awt.Image;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class Resources {
	
	// images
	public final static Image IMAGE_ICON = readImage("ic_icon.png");
	public final static Image IMAGE_PICTURE_NOT_AVAILABLE = readImage("pna.jpg");
	public final static Image IMAGE_LOGO = readImage("logo.png");
	
	// buttons
	// hover off
	public final static Image IMAGE_BUTTON_BACK = readButtonHoverOffImage("back.png");
	public final static Image IMAGE_BUTTON_LAYOUT = readButtonHoverOffImage("layout.png");
    public final static Image IMAGE_BUTTON_IMAGES = readButtonHoverOffImage("images.png");
    public final static Image IMAGE_BUTTON_MUSIC = readButtonHoverOffImage("music.png");
    public final static Image IMAGE_BUTTON_VIDEOS = readButtonHoverOffImage("videos.png");
    public final static Image IMAGE_BUTTON_SETTINGS = readButtonHoverOffImage("settings.png");
    public final static Image IMAGE_BUTTON_STREAM = readButtonHoverOffImage("stream.png");

    public final static Image IMAGE_BUTTON_STOP = readButtonHoverOffImage("stop.png");
    public final static Image IMAGE_BUTTON_PLAY = readButtonHoverOffImage("play.png");
    public final static Image IMAGE_BUTTON_PAUSE = readButtonHoverOffImage("pause.png");
    public final static Image IMAGE_BUTTON_MUTEON = readButtonHoverOffImage("muteon.png");
    public final static Image IMAGE_BUTTON_MUTEOFF = readButtonHoverOffImage("muteoff.png");

    // hover on
	public final static Image IMAGE_BUTTON_BACK_HOVER = readButtonHoverOnImage("back-hover.png");
	public final static Image IMAGE_BUTTON_LAYOUT_HOVER = readButtonHoverOnImage("layout-hover.png");
    public final static Image IMAGE_BUTTON_IMAGES_HOVER = readButtonHoverOnImage("images-hover.png");
    public final static Image IMAGE_BUTTON_MUSIC_HOVER = readButtonHoverOnImage("music-hover.png");
    public final static Image IMAGE_BUTTON_VIDEOS_HOVER = readButtonHoverOnImage("videos-hover.png");
    public final static Image IMAGE_BUTTON_SETTINGS_HOVER = readButtonHoverOnImage("settings-hover.png");
    
    public final static Image IMAGE_BUTTON_STOP_HOVER = readButtonHoverOnImage("stop-hover.png");
    public final static Image IMAGE_BUTTON_PLAY_HOVER = readButtonHoverOnImage("play-hover.png");
    public final static Image IMAGE_BUTTON_PAUSE_HOVER = readButtonHoverOnImage("pause-hover.png");
    public final static Image IMAGE_BUTTON_MUTEON_HOVER = readButtonHoverOnImage("muteon-hover.png");
    public final static Image IMAGE_BUTTON_MUTEOFF_HOVER = readButtonHoverOnImage("muteoff-hover.png");
    public final static Image IMAGE_BUTTON_STREAM_HOVER = readButtonHoverOnImage("stream-hover.png");

    public static ImageIcon imageToIcon(Image image) {
		return new ImageIcon(image);
	}

	public static Image readButtonHoverOnImage(String path) {
		try {
			return readButtonImage("hoveron/" + path);
		} catch (Exception e) {
			return null;
		}
	}

	public static Image readButtonHoverOffImage(String path) {
		try {
			return readButtonImage("hoveroff/" + path);
		} catch (Exception e) {
			return null;
		}
	}

	public static Image readButtonImage(String path) {
		try {
			return readImage("buttons/" + path);
		} catch (Exception e) {
			return null;
		}
	}

	public static Image readImage(String path) {
		try {
			return ImageIO.read(readResource("images/" + path));
		} catch (Exception e) {
			return null;
		}
	}
	
	// private
	private static InputStream readResource(String path) {
		try {
			return FileIO.class.getResourceAsStream("/resources/" + path);
		} catch (Exception e) {
			return null;
		}
	}
}
