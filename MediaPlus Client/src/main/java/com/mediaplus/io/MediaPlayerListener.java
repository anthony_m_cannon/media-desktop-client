package com.mediaplus.io;

import com.mediaplus.BaseObject;
import com.mediaplus.io.packet.command.CommandPacket;
import com.mediaplus.io.packet.command.CommandProperty;
import com.mediaplus.io.packet.command.CommandType;
import uk.co.caprica.vlcj.binding.internal.libvlc_media_t;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventListener;

public class MediaPlayerListener implements MediaPlayerEventListener {

    private long lastTime = -1;
    public static long totalTime = 0;

	public MediaPlayerListener() {
	}
	
	@Override
	public void mediaChanged(MediaPlayer mediaPlayer, libvlc_media_t media,
			String mrl) {
    }

	@Override
	public void opening(MediaPlayer mediaPlayer) {
    }

	@Override
	public void buffering(MediaPlayer mediaPlayer, float newCache) {
    }

	@Override
	public void playing(MediaPlayer mediaPlayer) {
        CommandPacket playPauseCommand = new CommandPacket(
                CommandType.SET_PROPERTY.getId()
                        + "0"
                        + CommandProperty.PAUSE_STATE
                        .getId()
                        + 1);
        playPauseCommand.sendPacket(BaseObject.client.getOutputStream());
	}

	@Override
	public void paused(MediaPlayer mediaPlayer) {
        CommandPacket playPauseCommand = new CommandPacket(
                CommandType.SET_PROPERTY.getId()
                        + "0"
                        + CommandProperty.PAUSE_STATE
                        .getId()
                        + 0);
        playPauseCommand.sendPacket(BaseObject.client.getOutputStream());
	}

	@Override
	public void stopped(MediaPlayer mediaPlayer) {
        System.out.println("stopped(" + mediaPlayer + ")");
	}

	@Override
	public void forward(MediaPlayer mediaPlayer) {
	}

	@Override
	public void backward(MediaPlayer mediaPlayer) {
	}

	@Override
	public void finished(MediaPlayer mediaPlayer) {
        System.out.println("finished(" + mediaPlayer + ")");
	}

	@Override
	public void timeChanged(MediaPlayer mediaPlayer, long newTime) {
        long now = System.currentTimeMillis();
        if(lastTime == -1){
            lastTime = now;
        }
        totalTime += (now - lastTime);
        
        lastTime = now;
    }

	@Override
	public void positionChanged(MediaPlayer mediaPlayer, float newPosition) {
	}

	@Override
	public void seekableChanged(MediaPlayer mediaPlayer, int newSeekable) {
	}

	@Override
	public void pausableChanged(MediaPlayer mediaPlayer, int newPausable) {
	}

	@Override
	public void titleChanged(MediaPlayer mediaPlayer, int newTitle) {
	}

	@Override
	public void snapshotTaken(MediaPlayer mediaPlayer, String filename) {
	}

	@Override
	public void lengthChanged(MediaPlayer mediaPlayer, long newLength) {
	}

	@Override
	public void videoOutput(MediaPlayer mediaPlayer, int newCount) {
	}

	@Override
	public void scrambledChanged(MediaPlayer mediaPlayer, int newScrambled) {
	}

	@Override
	public void elementaryStreamAdded(MediaPlayer mediaPlayer, int type, int id) {
	}

	@Override
	public void elementaryStreamDeleted(MediaPlayer mediaPlayer, int type,
			int id) {
	}

	@Override
	public void elementaryStreamSelected(MediaPlayer mediaPlayer, int type,
			int id) {
	}

	@Override
	public void error(MediaPlayer mediaPlayer) {
	}

	@Override
	public void mediaMetaChanged(MediaPlayer mediaPlayer, int metaType) {
	}

	@Override
	public void mediaSubItemAdded(MediaPlayer mediaPlayer,
			libvlc_media_t subItem) {
	}

	@Override
	public void mediaDurationChanged(MediaPlayer mediaPlayer, long newDuration) {
	}

	@Override
	public void mediaParsedChanged(MediaPlayer mediaPlayer, int newStatus) {
	}

	@Override
	public void mediaFreed(MediaPlayer mediaPlayer) {
	}

	@Override
	public void mediaStateChanged(MediaPlayer mediaPlayer, int newState) {
	}

	@Override
	public void newMedia(MediaPlayer mediaPlayer) {
	}

	@Override
	public void subItemPlayed(MediaPlayer mediaPlayer, int subItemIndex) {
	}

	@Override
	public void subItemFinished(MediaPlayer mediaPlayer, int subItemIndex) {
	}

	@Override
	public void endOfSubItems(MediaPlayer mediaPlayer) {
	}

}
