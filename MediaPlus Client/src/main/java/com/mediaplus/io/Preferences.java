package com.mediaplus.io;

import java.util.HashMap;

import com.mediaplus.types.HomeCategoryType;
import com.mediaplus.types.HomeLayoutType;
import com.mediaplus.utils.ConfigDefaults;

public class Preferences {
	private Config config;
	private HashMap<String, String> pref;
	private ConfigDefaults defaults;
	
	public Preferences() {
		config = new Config();
		pref = config.get();
		
		defaults = config.getDefaults();
	}
	
	// get
	public String getServerIp() {
		return pref.get("server-ip") != null ? pref.get("server-ip") : defaults.getServerIp();
	}
	
	public int getServerPort() {
		return Integer.valueOf(pref.get("server-port") != null ? pref.get("server-port") : defaults.getServerPort());
	}
	
	public int getStreamPort() {
		return Integer.valueOf(pref.get("stream-port") != null ? pref.get("stream-port") : defaults.getStreamPort());
	}
	
	public boolean getFullScreen() {
		return Boolean.valueOf(pref.get("fullScreen") != null ? pref.get("fullScreen") : defaults.getFullScreen());
	}
	
	public HomeCategoryType getCategory() {
		return HomeCategoryType.valueOf(pref.get("category") != null ? pref.get("category") : defaults.getCategory());
	}
	
	public HomeLayoutType getLayout() {
		return HomeLayoutType.valueOf(pref.get("layout") != null ? pref.get("layout") : defaults.getLayout());
	}
	
	// sets
	public void setServerIp(String ip) {
		pref.put("server-ip", ip);
	}
	
	public void setServerPort(int port) {
		pref.put("server-port", String.valueOf(port));
	}
	
	public void setStreamPort(int port) {
		pref.put("stream-port", String.valueOf(port));
	}
	
	public void setFullScreen(boolean fullScreen) {
		pref.put("fullScreen", String.valueOf(fullScreen));
	}
	
	public void setCategory(HomeCategoryType category) {
		pref.put("category", category.toString());
	}
	
	public void setLayout(HomeLayoutType layout) {
		pref.put("layout", layout.toString());
	}
	
	// other
	public boolean save() {
		return config.set(pref);
	}
}
