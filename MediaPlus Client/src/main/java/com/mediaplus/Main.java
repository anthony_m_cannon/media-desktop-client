package com.mediaplus;


import com.mediaplus.controllers.*;
import com.mediaplus.io.Preferences;
import com.mediaplus.models.*;
import com.mediaplus.types.ControllerType;

import javax.swing.*;

public class Main {
    private Controller currentController;

    public static void main(String[] args) {
        new Main();
    }

    public Main() {
        System.out.println("Desktop Client Started - Main (08)");

        String osName = System.getProperty("os.name").toLowerCase();
        boolean isMacOs = osName.startsWith("mac os x");
        if (isMacOs){
            JOptionPane.showMessageDialog(null, "Unfortunately Media+ is not supported on Mac OS X");
            System.exit(0);
        }

        BaseObject.bootstrap = this;
        BaseObject.pref = new Preferences();
        
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch(Exception e){
            e.printStackTrace();
        }

        if (!setRenderer()) {
            System.err.println("Failed to setup renderer.");
            System.exit(1);
        }
        
        load(false);
    }

    public void load(boolean reload){
        connectToServer(reload);

    }

    public void connectToServer(boolean reconnect) {
        loadController(getController(ControllerType.SPLASH));


		StartLoad load = new StartLoad(BaseObject.bootstrap, reconnect);
		Thread loadThread = new Thread(load, "LoadingThread");
        loadThread.start();
	}

	public Controller getController(ControllerType controllerName) {
        Model model;
        Controller controller;

        switch (controllerName) {
            default: // SPLASH
                model = new SplashModel();
                controller = new SplashController(model);
                break;
            case SPLASH:
                model = new SplashModel();
                controller = new SplashController(model);
                break;
            case HOME:
                model = new HomeModel();
                controller = new HomeController(model);
                break;
            case PLAYER:
                model = new PlayerModel();
                controller = new PlayerController(model);
                break;
            case CONFIG:
                model = new ConfigModel();
                controller = new ConfigController(model);
                break;
            case INFORMATION:
                model = new InformationModel();
                controller = new InformationController(model);
                break;
            case IMAGE:
                model = new ImageModel();
                controller = new ImageController(model);
                break;
        }

        return controller;
    }

    public boolean loadController(Controller c) {
        if (currentController == c) {
            return false;
        }

        currentController = c;

        return true;
    }

    public Controller getCurrentController() {
        return currentController;
    }

    // Private
    private boolean setRenderer() {
        Renderer renderer = new Renderer();
        BaseObject.renderer = renderer;
        boolean setUp = renderer.setupGUI();

        return setUp;
    }

}