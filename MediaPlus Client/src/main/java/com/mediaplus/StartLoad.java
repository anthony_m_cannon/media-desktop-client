package com.mediaplus;

import com.mediaplus.utils.LoadStatus;
import uk.co.caprica.vlcj.discovery.NativeDiscovery;

import com.mediaplus.io.Client;
import com.mediaplus.io.Preferences;
import com.mediaplus.io.packet.command.CommandPacket;
import com.mediaplus.io.packet.command.CommandType;
import com.mediaplus.media.MediaHandler;
import com.mediaplus.types.ControllerType;

import javax.swing.*;

public class StartLoad extends Thread {
    private Main main;
    private Client client;
    private boolean recreate;
    private static LoadStatus loadStatus = LoadStatus.PREPARING;

    public StartLoad(Main main, boolean recreate) {
        this.main = main;
        this.recreate = recreate;
    }

    @Override
    public void run() {

        createBaseObjects(recreate);
        if (!vlcFound()) {
            System.err.println("VLC not found.");
            JOptionPane.showMessageDialog(null, "VLC not found. Please make sure you have the correct VLC version installed");
            System.exit(1);
        }

        loadStatus = LoadStatus.CONNECTING;
        if (!attemptServerConnection()) {
            System.err.println("Could not connect to server.");
            JOptionPane.showMessageDialog(null, "Could not connect to server. Please check the configuration in config.ini.");
        	BaseObject.bootstrap.loadController(BaseObject.bootstrap.getController(ControllerType.CONFIG));
        } else {
            System.out.println("Connected successfully");
        }

//        BaseObject.client.sendRequest();
        BaseObject.mediaHandler.fetchData();

        Thread t = new Thread(new LoadFilms());
        t.start();


    }

    // Private
    private boolean vlcFound() {
        NativeDiscovery vlcFinder = new NativeDiscovery();
        boolean vlcFound = vlcFinder.discover();

        return vlcFound;
    }

    private boolean createBaseObjects(boolean reload) {

        if(reload){
            BaseObject.pref = new Preferences();
            BaseObject.mediaHandler = new MediaHandler();
            client = new Client();
            BaseObject.client = client;
        }else{
            if (BaseObject.pref == null) BaseObject.pref = new Preferences();
            if (BaseObject.mediaHandler == null) BaseObject.mediaHandler = new MediaHandler();

            if (client == null) client = new Client();
            if (BaseObject.client == null) BaseObject.client = client;
        }

        return true;
    }

    private boolean attemptServerConnection() {
        String ip = BaseObject.pref.getServerIp();
        int port = BaseObject.pref.getServerPort();
        
        if (client.isConnected()) {
        	client.stop();
        }
        
        return client.connect(ip, port);
    }

    public static LoadStatus getLoadStatus(){
        return loadStatus;
    }

    private class LoadFilms implements Runnable {

        @Override
        public void run() {
            loadStatus = LoadStatus.FILM;
            while (BaseObject.client.getNoFilms() != BaseObject.client.getLoadedMovies()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Thread t = new Thread(new LoadMusic());
            t.start();
        }
    }

    private class LoadMusic implements Runnable {

        @Override
        public void run() {
            loadStatus = LoadStatus.MUSIC;
            CommandPacket getMusic = new CommandPacket(CommandType.GET_MUSIC.getId() + "");
            getMusic.sendPacket(BaseObject.client.getOutputStream());

            while (BaseObject.client.getNoMusic() != BaseObject.client.getLoadedMusic()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            Thread t = new Thread(new LoadImages());
            t.start();
        }

    }

    private class LoadImages implements Runnable {

        @Override
        public void run() {
            loadStatus = LoadStatus.IMAGES;
            CommandPacket getPictures = new CommandPacket(CommandType.GET_IMAGES.getId() + "");
            getPictures.sendPacket(BaseObject.client.getOutputStream());

            while (BaseObject.client.getNoImages() != BaseObject.client.getLoadedImages()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            main.loadController(main.getController(ControllerType.HOME));
        }

    }
}
