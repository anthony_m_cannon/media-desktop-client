package com.mediaplus.utils;

public class ConfigDefaults {
	private final String[] key, value;

	public ConfigDefaults() {
		key = new String[6];
		value = new String[6];
		
		key[0] = "server-ip";
		key[1] = "server-port";
		key[2] = "stream-port";
		key[3] = "fullScreen";
		key[4] = "category";
		key[5] = "layout";
		
		value[0] = "10.170.167.206";
		value[1] = "4403";
		value[2] = "8080";
		value[3] = "true";
		value[4] = "FILMS";
		value[5] = "TILES";
	}

	public String getServerIp() {
		return value[0];
	}

	public String getServerPort() {
		return value[1];
	}

	public String getStreamPort() {
		return value[2];
	}

	public String getFullScreen() {
		return value[3];
	}

	public String getCategory() {
		return value[4];
	}

	public String getLayout() {
		return value[5];
	}

	public String[] keyToArray() {
		return new String[] { key[0], key[1], key[2], key[3],
				key[4], key[5] };
	}

	public String[] valueToArray() {
		return new String[] { value[0], value[1], value[2], value[3],
				value[4], value[5] };
	}

	public String[] toArray() {
		return new String[] { key[0] + "=" + value[0], key[1] + "=" + value[1], key[2] + "=" + value[2], key[3] + "=" + value[3],
				key[4] + "=" + value[4], key[5] + "=" + value[5] };
	}
}
