package com.mediaplus.utils;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JList;

import com.mediaplus.BaseObject;
import com.mediaplus.controllers.Controller;
import com.mediaplus.controllers.HomeController;
import com.mediaplus.controllers.ImageController;
import com.mediaplus.io.packet.command.CommandPacket;
import com.mediaplus.io.packet.command.CommandType;
import com.mediaplus.media.Picture;
import com.mediaplus.models.HomeModel;
import com.mediaplus.types.ControllerType;
import com.mediaplus.types.HomeCategoryType;

public class HomeItemListener implements MouseListener {
	public HomeItemListener() {

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		JList<?> list = (JList<?>) e.getSource();
		if (e.getClickCount() == 2) {
			Object result = list.getSelectedValue();
			if (result != null) {
				if (result instanceof String) {
					String chosen = (String) result;
					System.out.println(chosen);

                    Controller cont = BaseObject.bootstrap.getCurrentController();
                    if(cont instanceof HomeController) {
                        HomeController hc = (HomeController) cont;
                        HomeModel hm = hc.getModel();

                        if(hm.getCaregoryType() == HomeCategoryType.FILMS) {
                            System.out.println("CHOSEN MOVIE: " + chosen);
                            System.out.println("A: " + (BaseObject.mediaHandler.getFilmByTitle(chosen) == null));
                            System.out.println("B: " + (BaseObject.mediaHandler.getFilmByFileName(chosen) == null));

                            String fileName = BaseObject.mediaHandler.getFilmByTitle(chosen).getFileName();
                            CommandPacket getFilmInfo = new CommandPacket("0" + CommandType.INFO.getId() + "" + fileName);
                            getFilmInfo.sendPacket(BaseObject.client.getOutputStream());
                        }else if(hm.getCaregoryType() == HomeCategoryType.MUSIC){
                            CommandPacket commandPacket = new CommandPacket(CommandType.PLAY_SONG.getId() + "" + chosen);
                            commandPacket.sendPacket(BaseObject.client.getOutputStream());
                        }else if(hm.getCaregoryType() == HomeCategoryType.IMAGES){

                            ImageController imgController = (ImageController) BaseObject.bootstrap.getController(ControllerType.IMAGE);
                            Picture pic = BaseObject.mediaHandler.getPictureByTitle(chosen);
                            imgController.setImage(pic.getCover());

                            BaseObject.bootstrap.loadController(imgController);

                        }
                    }
					// get film and send to information
				}
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}
}
