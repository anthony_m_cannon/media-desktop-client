package com.mediaplus.utils;

/**
 * Created by Deividas on 27/03/2015.
 */
public enum LoadStatus {

    PREPARING, CONNECTING, FILM, MUSIC, IMAGES, INITIALISING;

}
