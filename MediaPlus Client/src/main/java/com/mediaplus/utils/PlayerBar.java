package com.mediaplus.utils;

import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import uk.co.caprica.vlcj.player.MediaPlayer;

import com.mediaplus.BaseObject;
import com.mediaplus.interfaces.ViewInterface;
import com.mediaplus.io.Resources;
import com.mediaplus.types.ButtonCommandType;

public class PlayerBar extends JPanel implements ViewInterface, Runnable {
	private static final long serialVersionUID = 1L;
	
	private JLabel pinTxt;
	private JButton playPause, stop, mute;
	private int width, height;
	
	private int pin;
	private boolean muted, playing;
	
	public PlayerBar(int width, int height, int pin) {
		this.width = width;
		this.height = height;
		
		muted = false;
		playing = false;
		
		this.pin = pin;
	}

	@Override
	public void createView() {
		createElements();
		setElementsAdditionalInfo();
		setElementsSize();
		setElementsLocation();
		addElements();
	}

	@Override
	public void createElements() {
		playPause = new JButton();
        stop = new JButton();
        mute = new JButton();
        
        pinTxt = new JLabel("Pin: " + String.valueOf(pin));
	}

	@Override
	public void setElementsAdditionalInfo() {
		setBackground(Color.gray);
		
		pinTxt.setForeground(Color.white);
		
		playPause.setContentAreaFilled(false);
		stop.setContentAreaFilled(false);
		mute.setContentAreaFilled(false);
		
		playPause.setBorder(BorderFactory.createEmptyBorder());
		stop.setBorder(BorderFactory.createEmptyBorder());
		mute.setBorder(BorderFactory.createEmptyBorder());
		
		stop.setIcon(Resources.imageToIcon(Resources.IMAGE_BUTTON_STOP));
		stop.setRolloverIcon(Resources.imageToIcon(Resources.IMAGE_BUTTON_STOP_HOVER));
		
		playPause.setIcon(Resources.imageToIcon(Resources.IMAGE_BUTTON_PAUSE));
		playPause.setRolloverIcon(Resources.imageToIcon(Resources.IMAGE_BUTTON_PAUSE_HOVER));

		mute.setIcon(Resources.imageToIcon(Resources.IMAGE_BUTTON_MUTEOFF));
		mute.setRolloverIcon(Resources.imageToIcon(Resources.IMAGE_BUTTON_MUTEOFF_HOVER));
	}

	@Override
	public void setElementsSize() {
		playPause.setSize(playPause.getPreferredSize());
        stop.setSize(stop.getPreferredSize());
        mute.setSize(mute.getPreferredSize());
        pinTxt.setSize(pinTxt.getPreferredSize());
	}

	@Override
	public void setElementsLocation() {
		playPause.setLocation((width - playPause.getWidth()) / 2, (height - playPause.getHeight()) / 2);
		stop.setLocation(playPause.getX() + playPause.getWidth() + 10, playPause.getY() + playPause.getHeight() + 10);
		mute.setLocation(stop.getX() + stop.getWidth() + 10, stop.getY() + stop.getHeight() + 10);
	}

	@Override
	public void addElements() {
        add(playPause);
        add(stop);
        add(mute);
        
        add(pinTxt);
	}
	
	public void addActionListener(ActionListener listener) {
		playPause.setActionCommand(ButtonCommandType.PLAYPAUSE.id());
		stop.setActionCommand(ButtonCommandType.STOP.id());
		mute.setActionCommand(ButtonCommandType.MUTE.id());
		
		playPause.addActionListener(listener);
		stop.addActionListener(listener);
		mute.addActionListener(listener);
	}
	
	public void start() {
		Thread t = new Thread(this);
		t.start();
	}

	@Override
	public void run() {
		MediaPlayer player = BaseObject.player;
		
		while (true) {
			if (player.isPlaying() != playing) togglePlaying();
			if (player.isMute() != muted) toggleMute();
		}
	}
	
	private void togglePlaying() {
		if (playing) {
			playPause.setIcon(Resources.imageToIcon(Resources.IMAGE_BUTTON_PLAY));
			playPause.setRolloverIcon(Resources.imageToIcon(Resources.IMAGE_BUTTON_PLAY_HOVER));
		} else {
			playPause.setIcon(Resources.imageToIcon(Resources.IMAGE_BUTTON_PAUSE));
			playPause.setRolloverIcon(Resources.imageToIcon(Resources.IMAGE_BUTTON_PAUSE_HOVER));
		}
		playPause.updateUI();
		playing = !playing;
	}
	
	private void toggleMute() {
		if (muted) {
			mute.setIcon(Resources.imageToIcon(Resources.IMAGE_BUTTON_MUTEOFF));
			mute.setRolloverIcon(Resources.imageToIcon(Resources.IMAGE_BUTTON_MUTEOFF_HOVER));
		} else {
			mute.setIcon(Resources.imageToIcon(Resources.IMAGE_BUTTON_MUTEON));
			mute.setRolloverIcon(Resources.imageToIcon(Resources.IMAGE_BUTTON_MUTEON_HOVER));
		}
		mute.updateUI();
		muted = !muted;
	}

}
