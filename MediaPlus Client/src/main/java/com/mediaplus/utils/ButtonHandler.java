package com.mediaplus.utils;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import com.mediaplus.io.Resources;
import com.mediaplus.types.ButtonCommandType;

public class ButtonHandler {
	public ButtonHandler() {
		
	}

	public JButton getViewBtn() {
		ImageIcon ii = Resources.imageToIcon( Resources.IMAGE_BUTTON_LAYOUT );
		JButton btn = new JButton(ii);
		btn.setActionCommand(ButtonCommandType.CHANGE_VIEW.id());
		btn.setRolloverIcon(Resources.imageToIcon( Resources.IMAGE_BUTTON_LAYOUT_HOVER ));
		
		return btn;
	}

	public JButton getImageBtn() {
		JButton btn = new JButton();
		btn.setActionCommand(ButtonCommandType.VIEW_IMAGE.id());
		
		return btn;
	}

	public JButton getMusicBtn() {
		JButton btn = new JButton();
		btn.setActionCommand(ButtonCommandType.VIEW_MUSIC.id());
		
		return btn;
	}

	public JButton getFilmBtn() {
		JButton btn = new JButton();
		btn.setActionCommand(ButtonCommandType.VIEW_FILM.id());
		
		return btn;
	}
}
