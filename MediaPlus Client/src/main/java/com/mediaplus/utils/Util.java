package com.mediaplus.utils;

/**
 * Created by Deividas on 25/03/2015.
 */
public class Util {

    public static int getPercentage(int max, int current){
        return (int)((current * 100.0f) / max);
    }

}
