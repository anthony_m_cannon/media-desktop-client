package com.mediaplus.utils;

import java.awt.Rectangle;

public class Window {
	private int width, height;
	
	public Window(Rectangle guiBounds) {
		width = (int) guiBounds.getWidth();
		height = (int) guiBounds.getHeight();
	}
	
	public int width() {
		return width;
	}
	
	public int height() {
		return height;
	}
}
