package com.mediaplus;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.mediaplus.io.Resources;
import com.mediaplus.utils.Window;
import com.mediaplus.views.View;

/**
 * Created by Deividas on 17/11/2014.
 */
public class Renderer implements Runnable {

	private JFrame frame;
	private View currentView;

	private static final String TITLE = "Media Network";
	public Window GUI_SIZE;
	private static boolean guiCreated = false;

	private GraphicsEnvironment graphicsAdapter;
	private GraphicsDevice monitor;

	/**
	 * Set up and start GUI
	 */
	public boolean setupGUI() {
		if (guiCreated) {
			return false;
		}

		graphicsAdapter = GraphicsEnvironment.getLocalGraphicsEnvironment();
		monitor = graphicsAdapter.getDefaultScreenDevice();

		GUI_SIZE = new Window(monitor.getDefaultConfiguration().getBounds());

        try {
            SwingUtilities.invokeAndWait(this);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return true;
	}
	
	public Window GUI() {
		return GUI_SIZE;
	}
	
	public void showDialog(JPanel panel) {
        
    }
	
	public void exitDialog(JPanel panel) {
		
	}
	
	public View getCurrentView() {
		return currentView;
	}

	public void setView(View view) {

		if (view == null) {
			throw new NullPointerException(
					"The view that you're trying to set cannot be null.");
		}

		if (!guiCreated) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if (view != null && currentView != view) { 
			currentView = view;
			frame.getContentPane().removeAll();
            frame.getContentPane().setBackground(Color.BLACK);
			if (currentView != null) {
                if(currentView.header != null) {
                    frame.getContentPane().add(currentView.header, BorderLayout.NORTH);
                }
				frame.getContentPane().add(currentView, BorderLayout.CENTER);
			}
			frame.getContentPane().invalidate();
			frame.getContentPane().validate();
			frame.repaint();
		} else {
			System.out.println("Could not set view");
		}
	}

	public JFrame getFrame() {
		return frame;
	}

	public boolean setFullScreen(GraphicsDevice screen) {
		if (screen.isFullScreenSupported()) {
			if (BaseObject.pref.getFullScreen()) {
				frame.setUndecorated(true);
			}
			screen.setFullScreenWindow(frame);
			return true;
		}
		return false;
	}
	
	@Override
	public void run() {
		frame = new JFrame();

		frame.setSize(GUI_SIZE.width(), GUI_SIZE.height());
		frame.setLayout(new BorderLayout());
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setTitle(TITLE);

		setFullScreen(monitor);
        if(Resources.IMAGE_ICON != null) {
            frame.setIconImage(new ImageIcon(Resources.IMAGE_ICON).getImage());
        }
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setVisible(true);

	}

}
