package com.mediaplus.types;

public enum HomeCategoryType {
	IMAGES(0), MUSIC(1), FILMS(2);
	
	private int id;
	
	private HomeCategoryType(int id) {
		this.id = id;
	}
	
	public static String[] getAll() {
		return new String[] {"IMAGES", "MUSIC", "FILMS"};
	}
	
	public static HomeCategoryType get(int id) {
		switch(id) {
		default:
		case 0:
			return IMAGES;
		case 1:
			return MUSIC;
		case 2:
			return FILMS;
		}
	}
	
	public int id() {
		return id;
	}
}
