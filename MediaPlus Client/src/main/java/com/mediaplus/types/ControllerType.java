package com.mediaplus.types;


public enum ControllerType {
	SPLASH, HOME, PLAYER, CONFIG, INFORMATION, IMAGE;
}