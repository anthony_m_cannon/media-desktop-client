package com.mediaplus.types;

public enum HomeLayoutType {
	ICONS("Icons", 0), LIST("List", 1), TILES("Tiles", 2);
	
	private String name;
	private int id;

	HomeLayoutType(String name, int id) {
		this.name = name;
		this.id = id;
	}

	public static HomeLayoutType getPacketType(String id) {
		if (id.equals("Icons")) {
			return ICONS;
		} else if (id.equals("List")) {
			return LIST;
		} else if (id.equals("Tiles")) {
			return TILES;
		} else {
			return ICONS;
		}
	}
	
	public static HomeLayoutType get(int id) {
		switch(id) {
		default:
		case 0:
			return ICONS;
		case 1:
			return LIST;
		case 2:
			return TILES;
		}
	}
	
	public int id() {
		return id;
	}

	public String nameId() {
		return name;
	}
	
	public static String[] getAll() {
		return new String[] {"Icons", "List", "Tiles"};
	}
}
