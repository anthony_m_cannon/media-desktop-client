package com.mediaplus.types;

public enum ButtonCommandType {
	OPEN_SPLASH("OPEN_SPLASH"), OPEN_HOME("OPEN_HOME"), OPEN_CONFIG("OPEN_CONFIG"), CHANGE_VIEW(
			"CHANGE_VIEW"), VIEW_IMAGE("VIEW_IMAGE"), VIEW_MUSIC("VIEW_MUSIC"), VIEW_FILM("VIEW_FILM"), BACK(
			"BACK"), CONNECT("CONNECT"), CATEGORY("CATEGORY"), LAYOUT("LAYOUT"), PLAYPAUSE("PLAYPAUSE"), STOP("STOP"), MUTE("MUTE");

	private String id;

	ButtonCommandType(String id) {
		this.id = id;
	}

	public static ButtonCommandType getButtonType(String id) {
		if (id.equals("OPEN_SPLASH")) {
			return OPEN_SPLASH;
		} else if (id.equals("OPEN_HOME")) {
			return OPEN_HOME;
		} else if (id.equals("OPEN_CONFIG")) {
			return OPEN_CONFIG;
		} else if (id.equals("CHANGE_VIEW")) {
			return CHANGE_VIEW;
		} else if (id.equals("VIEW_IMAGE")) {
			return VIEW_IMAGE;
		} else if (id.equals("VIEW_MUSIC")) {
			return VIEW_MUSIC;
		} else if (id.equals("VIEW_FILM")) {
			return VIEW_FILM;
		} else if (id.equals("BACK")) {
			return BACK;
		} else if (id.equals("CONNECT")) {
			return CONNECT;
		} else if (id.equals("CATEGORY")) {
			return CATEGORY;
		}  else if (id.equals("LAYOUT")) {
			return LAYOUT;
		} else if (id.equals("PLAYPAUSE")) {
			return PLAYPAUSE;
		} else if (id.equals("STOP")) {
			return STOP;
		}  else if (id.equals("MUTE")) {
			return MUTE;
		} else {
			return OPEN_HOME;
		}
	}

	public String id() {
		return id;
	}
}
