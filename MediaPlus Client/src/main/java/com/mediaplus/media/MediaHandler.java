package com.mediaplus.media;

import java.awt.Image;
import java.util.ArrayList;

public class MediaHandler {
	private ArrayList<Film> films;
	private ArrayList<Song> songs;
	private ArrayList<Picture> pictures;

    public static int loadedFilms = 0;

	public MediaHandler() {
		films = new ArrayList<Film>();
		songs = new ArrayList<Song>();
		pictures = new ArrayList<Picture>();
	}
	
	public void fetchData() {
		
	}

	// Get All
	public ArrayList<Picture> getAllPictures() {
		return pictures;
	}

	public ArrayList<Song> getAllSongs() {
		return songs;
	}

	public ArrayList<Film> getAllFilms() {
		return films;
	}

	// Get ByTitle
	public Picture getPictureByTitle(String title) {
		for (Picture picture : pictures) {
			if (title.equals(picture.getTitle())) {
				return picture;
			}
		}

		return null;
	}

	public Song getSongByTitle(String title) {
		for (Song song : songs) {
			if (title.equals(song.getTitle())) {
				return song;
			}
		}

		return null;
	}

    public Film getFilmByFileName(String fileName) {
        for (Film film : films) {
            if (fileName.equals(film.getFileName())) {
                return film;
            }
        }

        return null;
    }

	public Film getFilmByTitle(String title) {
		for (Film film : films) {
			if (title.equals(film.getTitle())) {
				return film;
			}
		}

		return null;
	}

	// New
	public Picture newPicture(Image cover, String title) {
		Picture picture = new Picture(cover, title);
		pictures.add(picture);

		return picture;
	}

	public Song newSong(Image cover, String title, String extention) {
		Song song = new Song(cover, title, extention);
		songs.add(song);

		return song;
	}

	public Film newFilm(Image cover, String title, String extention) {
		Film film = new Film(cover, title, extention);
		films.add(film);

		return film;
	}

	// Add
	public void addInfoToFilm(String title, int year, int runTime, String genre, double iRating, double rRating, String[] directors, String[] actors, String storyLine) {
		Film film = getFilmByTitle(title);
		
		if (film != null) {
			film.addInfo(year, runTime, genre, iRating, rRating, directors, actors, storyLine);
            loadedFilms++;
		}
	}
	
	public void addInfoToSong(String title, int year, int runTime, String genre, String artist, String producer, String album, String[] featuring) {
		Song song = getSongByTitle(title);
		
		if (song != null) {
			song.addInfo(year, runTime, genre, artist, producer, album, featuring);
		}
	}
}
