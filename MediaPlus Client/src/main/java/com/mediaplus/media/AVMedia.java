package com.mediaplus.media;

import java.awt.Image;

public class AVMedia extends Media {
	private boolean hasInfo;
	private String genre, extention;
	private int year, runTime;
	
	public AVMedia(Image poster, String title, String extention) {
		super(poster, title);
		
		hasInfo = false;
		this.extention = extention;
	}
	
	protected void addInfo(int year, int runTime, String genre) {
		this.year = year;
		this.runTime = runTime;
		this.genre = genre;
		
		hasInfo = true;
	}
	
	// Gets
	public int getYear() {
		return year;
	}
	
	public int getRunTime() {
		return runTime;
	}
	
	public String getGenre() {
		return genre;
	}
	
	public boolean hasInfo() {
		return hasInfo;
	}
	
	public String getExtention() {
		return extention;
	}
	
	public String getFileName() {
		return getTitle() + getExtention();
	}

    public void setYear(int year) {
        this.year = year;
    }

    public void setRunTime(int runTime) {
        this.runTime = runTime;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
