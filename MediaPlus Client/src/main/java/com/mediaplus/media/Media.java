package com.mediaplus.media;

import javax.imageio.ImageIO;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;

public class Media {
	private Image cover;
	private String title;
	
	public Media(Image poster, String title) {
		this.cover = poster;
		this.title = title;
	}
	
	// Gets
	public Image getCover() {
		return cover;
	}
	
	public String getTitle() {
		return title;
	}

    public void setPoster(final String posterURL){
        if(posterURL.equals("N/A")) return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    cover = ImageIO.read(new URL(posterURL));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
