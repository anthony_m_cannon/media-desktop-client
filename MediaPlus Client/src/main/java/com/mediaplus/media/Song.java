package com.mediaplus.media;

import java.awt.Image;

public class Song extends AVMedia {
	private String artist, producer, album;
	private String[] featuring;
	
	public Song(Image cover, String title, String extention) {
		super(cover, title, extention);
	}
	
	public void addInfo(int year, int runTime, String genre, String artist, String producer, String album, String[] featuring) {
		super.addInfo(year, runTime, genre);
		
		this.artist = artist;
		this.producer = producer;
		this.album = album;
		this.featuring = featuring;
	}
	
	// Gets
	public String getArtist() {
		return artist;
	}
	
	public String getProducer() {
		return producer;
	}
	
	public String getAlbum() {
		return album;
	}
	
	public String[] getFeaturing() {
		return featuring;
	}

}
