package com.mediaplus.media;

import java.awt.Image;
import java.util.Arrays;

public class Film extends AVMedia {
	private double IMDBRating, RTRating, overAllRating = 0.0;
	private String[] directors, actors = {""};
	private String storyLine = "";
    private String country;

    public Film(Image poster, String title, String extention) {
        super(poster, title, extention);
    }
	
	public void addInfo(int year, int runTime, String genre, double iRating, double rRating, String[] directors, String[] actors, String storyLine) {
		super.addInfo(year, runTime, genre);
		
		this.IMDBRating = iRating;
		this.RTRating = rRating;
		overAllRating = workOutOverAllRating();
		this.directors = directors;
		this.actors = actors;
		this.storyLine = storyLine;

        System.out.println("test: " + this);
    }
	
	// Gets
	public double getIMDBRating() {
		return IMDBRating;
	}
	
	public double getRTRating() {
		return RTRating;
	}
	
	public double getOverAllRating() {
		return overAllRating;
	}
	
	public String[] getDirectors() {
		return directors;
	}
	
	public String[] getActors() {
		return actors;
	}
	
	public String getStoryLine() {
		return storyLine;
	}
	
	// Private functions
	private double workOutOverAllRating() {
		return (IMDBRating + RTRating) / 2;
	}

    @Override
    public String toString() {
        return "Film{" +
                "IMDBRating=" + IMDBRating +
                ", RTRating=" + RTRating +
                ", overAllRating=" + overAllRating +
                ", directors=" + Arrays.toString(directors) +
                ", actors=" + Arrays.toString(actors) +
                ", storyLine='" + storyLine + '\'' +
                '}';
    }

    public void setImdbRating(double imdbRating) {
        this.IMDBRating = imdbRating;
    }

    public void setTomatoRating(double tomatoRating) {
        this.RTRating = tomatoRating;
    }

    public void setDirectors(String[] directors) {
        this.directors = directors;
    }

    public void setActors(String[] actors) {
        this.actors = actors;
    }

    public void setPlot(String plot) {
        this.storyLine = plot;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
