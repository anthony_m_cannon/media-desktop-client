package com.mediaplus.interfaces;

public interface ViewInterface {
	
	public void createView();
	public abstract void createElements();
	public abstract void setElementsAdditionalInfo();
	public abstract void setElementsSize();
	public abstract void setElementsLocation();
	public abstract void addElements();
	
}
