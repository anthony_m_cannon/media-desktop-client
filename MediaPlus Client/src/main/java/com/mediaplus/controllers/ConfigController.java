package com.mediaplus.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JOptionPane;

import com.mediaplus.BaseObject;
import com.mediaplus.models.ConfigModel;
import com.mediaplus.models.Model;
import com.mediaplus.types.ButtonCommandType;
import com.mediaplus.types.ControllerType;
import com.mediaplus.views.ConfigView;

public class ConfigController extends Controller implements ActionListener, ItemListener {
	private ConfigModel model;
	private ConfigView view;

	public ConfigController(Model model) {
		super(model);
		
		this.model = (ConfigModel) model;
		
		createView();
	}
	
	@Override
	public void createView() {
		view = new ConfigView(model.getWindowWidth(), model.getWindowHeight(), model.getIp(), model.getPortTxt(), model.getLogo(), model.getFullScreen(), model.getLayoutId(), model.getCategoryId());
    	setAuthority();
    	setView(view);
	}
	
	@Override
	public void setAuthority() {
		this.view.addActionListeners(this);
	}
	
	// listeners
	// action
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        
        ButtonCommandType btnCommand = ButtonCommandType.getButtonType(command);
        switch(btnCommand) {
		case BACK:
			BaseObject.bootstrap.loadController(BaseObject.bootstrap.getController(ControllerType.HOME));
			break;
		case CONNECT:
			String ipField = view.getIpField();
			int portField = Integer.parseInt(view.getPortField());
			
			model.setIp(ipField);
			model.setPort(portField);

			BaseObject.bootstrap.connectToServer(true);
			break;
		case CATEGORY:
			int categoryId = view.getCategoryId();
			model.setCategoryId(categoryId);
			break;
		case LAYOUT:
			int layoutId = view.getLayoutId();
			model.setLayoutId(layoutId);
			break;
		default:
			break;
        
        }
    }
    
    // item
	@Override
	public void itemStateChanged(ItemEvent e) {
		model.setFullScreen( view.getFullScreen() );
		JOptionPane.showMessageDialog(null, "Media+ is about to close for the changes to take effect. Please start Media+ again.");
		System.exit(1);
	}
}
