package com.mediaplus.controllers;

import com.mediaplus.BaseObject;
import com.mediaplus.models.Model;
import com.mediaplus.views.View;

/**
 * Created by Deividas on 14/11/2014.
 */
public class Controller {

	protected Model model;
	protected View view;

	public Controller(Model model) {
		this.model = model;
	}

	public Controller(Model model, View view) {
		System.out.println("Creating controller " + view + " " + model);
		
		this.model = model;
		this.view = view;

		this.setView(view);
	}
	
//	public void setKeyListener(View view) {
//		view.addKeyListener(this);
//	}

	protected void setView(View view) {
//		setKeyListener(view);
		BaseObject.renderer.setView(view);
	}
	
	public void createView() {}
	public void setAuthority() {};

}
