package com.mediaplus.controllers;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.mediaplus.BaseObject;
import com.mediaplus.models.ImageModel;
import com.mediaplus.models.Model;
import com.mediaplus.types.ControllerType;
import com.mediaplus.views.ImageView;

/**
 * Created by Deividas on 26/03/2015.
 */
public class ImageController extends Controller implements ActionListener{

    private ImageModel model;
    private ImageView view;
    public Image image;

    public ImageController(Model model) {
        super(model);

        this.model = (ImageModel) model;
    }

    @Override
    public void createView() {
        if(image != null) {
            view = new ImageView(model.getWindowWidth(), model.getWindowHeight(), image);
            setAuthority();
            setView(view);
            System.out.println("SHOWING VIEW");
        }
    }

    @Override
    public void setAuthority() {
        this.view.addActionListeners(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if(command.equals("BACK")){
            BaseObject.bootstrap.loadController(BaseObject.bootstrap.getController(ControllerType.HOME));
        }
    }

    public void setImage(Image img){
        this.image = img;
        System.out.println("IMAGE SHOULD BE SET");
        createView();
    }
}
