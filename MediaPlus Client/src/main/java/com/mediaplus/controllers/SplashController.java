package com.mediaplus.controllers;

import com.mediaplus.models.Model;
import com.mediaplus.models.SplashModel;
import com.mediaplus.views.SplashView;

public class SplashController extends Controller {
	private SplashModel model;
	private SplashView view;
	
	public SplashController(Model model) {
		super(model);

		this.model = (SplashModel) model;

		createView();
	}

	@Override
	public void createView() {
		view = new SplashView(model.getWindowWidth(), model.getWindowHeight());
		
		setView(view);
	}

}
