package com.mediaplus.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.mediaplus.BaseObject;
import com.mediaplus.Main;
import com.mediaplus.io.MediaPlayerListener;
import com.mediaplus.models.Model;
import com.mediaplus.models.PlayerModel;
import com.mediaplus.types.ButtonCommandType;
import com.mediaplus.types.ControllerType;
import com.mediaplus.views.PlayerView;

public class PlayerController extends Controller implements ActionListener {

	private PlayerModel playerModel;
	private PlayerView playerView;

    public PlayerController(Model model) {
		super(model);

		this.playerModel = (PlayerModel) model;
		playerModel.getPlayerComponent().getMediaPlayer()
				.addMediaPlayerEventListener(new MediaPlayerListener());

        view = new PlayerView(model.getWindowWidth(), model.getWindowHeight(), BaseObject.client.getDesktopPin());

		playerView = (PlayerView) view;
		playerView.setMediaCompontent(playerModel.getPlayerComponent());
		playerView.finalise();
		playerView.addActionListener(this);
		
		this.setView(playerView);
	}
	
	public void play(){
		playerModel.play();
	}

    public void pause(){
		playerModel.pause();
	}

    public void loadVideo(String mrl) {
        playerModel.loadVideo("http://" + mrl);

    }

	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		ButtonCommandType commandType = ButtonCommandType.getButtonType(command);
		System.out.println("commandType: " + commandType);
		switch(commandType) {
		case MUTE:
			BaseObject.player.mute();
			break;
		case PLAYPAUSE:
			if (BaseObject.player.isPlaying()) {
				BaseObject.player.pause();
			} else {
				BaseObject.player.play();
			}
			break;
		case STOP:
			BaseObject.player.stop();
			
			Main main = BaseObject.bootstrap;
			main.loadController(main.getController(ControllerType.HOME));
			break;
		default:
			
			break;
		
		}
	}
}