package com.mediaplus.controllers;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import com.mediaplus.BaseObject;
import com.mediaplus.Main;
import com.mediaplus.media.Film;
import com.mediaplus.media.Picture;
import com.mediaplus.media.Song;
import com.mediaplus.models.HomeModel;
import com.mediaplus.models.Model;
import com.mediaplus.types.ButtonCommandType;
import com.mediaplus.types.ControllerType;
import com.mediaplus.types.HomeCategoryType;
import com.mediaplus.views.HomeView;

/**
 * Created by Deividas on 14/11/2014.
 */
public class HomeController extends Controller implements ActionListener  {
	private HomeModel model;
	private HomeView view;

	public HomeController(Model model) {
		super(model);

		this.model = (HomeModel) model;

		createView();
	}

	@Override
	public void createView() {
		ArrayList<String> titles = new ArrayList<String>();
		ArrayList<Image> covers = new ArrayList<Image>();
		switch(model.getCaregoryType()) {
		case FILMS:
            if(model.getFilms() != null) {
                for (Film film : model.getFilms()) {
                    titles.add(film.getTitle());
                    covers.add(film.getCover());
                }
            }
			break;
		case IMAGES:
            if(model.getPictures() != null) {
                for (Picture picture : model.getPictures()) {
                    titles.add(picture.getTitle());
                    covers.add(picture.getCover());
                }
            }
			break;
		case MUSIC:
            if(model.getSongs() != null) {
                for (Song song : model.getSongs()) {
                    titles.add(song.getTitle());
                    covers.add(song.getCover());
                }
            }
			break;
		default:
			break;
		}
		
		view = new HomeView(model.getCaregoryType(), model.getLayoutType(), titles, covers, model.getWindowWidth(), model.getWindowHeight());
    	setAuthority();
    	setView(view);
	}

	@Override
	public void setAuthority() {
		this.view.addActionListeners(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ButtonCommandType command = ButtonCommandType.getButtonType(e.getActionCommand());
		Main main;

        switch(command) {
		default: // if wrong command - restart app
			main = BaseObject.bootstrap;
        	main.loadController( main.getController(ControllerType.SPLASH) );
			break;
        case CHANGE_VIEW:
        	model.setLayoutNumber( model.nextLayout() );
        	createView();
        	break;
        case OPEN_CONFIG:
        	main = BaseObject.bootstrap;
        	main.loadController( main.getController(ControllerType.CONFIG) );
        	break;
        case VIEW_IMAGE:
        	model.setCaregoryType(HomeCategoryType.IMAGES);
        	// set titles and covers before create
        	createView();
        	break;
		case VIEW_MUSIC:
			model.setCaregoryType(HomeCategoryType.MUSIC);
        	// set titles and covers before create
        	createView();
			break;
		case VIEW_FILM:
			model.setCaregoryType(HomeCategoryType.FILMS);
        	// set titles and covers before create
        	createView();
			break;
        }
	}

    public HomeModel getModel() {
        return model;
    }
}
