package com.mediaplus.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.mediaplus.BaseObject;
import com.mediaplus.io.packet.command.CommandPacket;
import com.mediaplus.io.packet.command.CommandType;
import com.mediaplus.media.Film;
import com.mediaplus.models.InformationModel;
import com.mediaplus.models.Model;
import com.mediaplus.types.ButtonCommandType;
import com.mediaplus.types.ControllerType;
import com.mediaplus.views.InformationView;

public class InformationController extends Controller implements ActionListener {
    private InformationModel model;
    private InformationView view;
    private String film;

    public InformationController(Model model) {
        super(model);

        this.model = (InformationModel) model;
        createView();
    }

    @Override
    public void createView() {
        view = new InformationView(model.getWindowWidth(), model.getWindowHeight());
        setAuthority();
        setView(view);
    }

    @Override
    public void setAuthority() {

        this.view.addActionListeners(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        ButtonCommandType command = ButtonCommandType.getButtonType(e
                .getActionCommand());

        switch (command) {
            default:

                break;
            case CHANGE_VIEW:
                BaseObject.bootstrap.loadController(BaseObject.bootstrap.getController(ControllerType.HOME));
                break;
            case VIEW_FILM:
                System.out.println("VIEWING FILM: " + film);
                Film f = BaseObject.mediaHandler.getFilmByFileName(film);
                if(f == null){
                    f = BaseObject.mediaHandler.getFilmByTitle(film);
                }
                String fileName = f.getFileName();
                System.out.println("Attempting to stream [" + fileName + "]");
                CommandPacket commandPacket = new CommandPacket(CommandType.STREAM_FILM.getId() + "" + fileName);
                commandPacket.sendPacket(BaseObject.client.getOutputStream());
                break;
        }
    }

    public void setFilm(String film, boolean askInfo) {
        this.film = film;
        Film f = BaseObject.mediaHandler.getFilmByTitle(film);
        String fileName = film;
        if(f != null){
            fileName = f.getFileName();
        }
        System.out.println("GETTING MOVIE FOR THE FOLLOWING FILE NAME: " + fileName);
        view.setFilm(fileName);
    }

    public InformationView getView() {
        return view;
    }
}
