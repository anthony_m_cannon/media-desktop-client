package com.mediaplus.models;

import java.awt.Image;

import com.mediaplus.BaseObject;
import com.mediaplus.io.Resources;

/**
 * Created by Deividas on 14/11/2014.
 */
public class Model {

	public Model() {
		
	}

	public int getWindowWidth() {

		return BaseObject.renderer.GUI_SIZE.width();
	}

	public int getWindowHeight() {
		
		return BaseObject.renderer.GUI_SIZE.height();
	}
	
	public Image getLogo() {
		return Resources.IMAGE_LOGO;
	}

}
