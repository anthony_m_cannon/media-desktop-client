package com.mediaplus.models;

import java.util.ArrayList;

import com.mediaplus.BaseObject;
import com.mediaplus.media.Film;
import com.mediaplus.media.Picture;
import com.mediaplus.media.Song;
import com.mediaplus.types.HomeCategoryType;
import com.mediaplus.types.HomeLayoutType;

/**
 * Created by Deividas on 14/11/2014.
 */
public class HomeModel extends Model {
	private int layoutNumber;
	private HomeCategoryType categoryType;

	public HomeModel() {
		super();

		layoutNumber = 0;
		categoryType = HomeCategoryType.FILMS;

	}
	
	public int nextLayout() {
		if (layoutNumber < (getLayoutCount())) {
			layoutNumber++;
		} else {
			layoutNumber = 0;
		}
		
		return layoutNumber;
	}
	
	// Sets
	public void setCaregoryType(HomeCategoryType hct) {
		categoryType = hct;
	}

	public void setLayoutNumber(int layoutNumber) {
		this.layoutNumber = layoutNumber;
	}
	
	// Gets
	public HomeLayoutType getLayoutType() {
		int i=0;
		for (HomeLayoutType hlt : HomeLayoutType.values()) {
			if (i == layoutNumber) {
				return hlt;
			}
			i++;
		}
		
		return HomeLayoutType.ICONS;
	}

	public HomeCategoryType getCaregoryType() {
		return categoryType;
	}

	public ArrayList<Film> getFilms() {
        if(BaseObject.mediaHandler == null) return null;
		return BaseObject.mediaHandler.getAllFilms();
	}

	public ArrayList<Song> getSongs() {
		return BaseObject.mediaHandler.getAllSongs();
	}

	public ArrayList<Picture> getPictures() {
		return BaseObject.mediaHandler.getAllPictures();
	}
	
	public int getLayoutNumber() {
		return layoutNumber;
	}
	
	// Private
	
	private int getLayoutCount() {
		int length = HomeLayoutType.values().length;
		
		return length;
	}
}
