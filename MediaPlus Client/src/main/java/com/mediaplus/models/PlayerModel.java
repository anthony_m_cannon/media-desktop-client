package com.mediaplus.models;

import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;
import uk.co.caprica.vlcj.player.MediaPlayer;

import com.mediaplus.BaseObject;

public class PlayerModel extends Model {

	private EmbeddedMediaPlayerComponent mediaPlayerComponent;
	private MediaPlayer player;

	public PlayerModel() {
		super();
		
		mediaPlayerComponent = new EmbeddedMediaPlayerComponent();

		player = mediaPlayerComponent.getMediaPlayer();
        BaseObject.player = player;
	}

	public EmbeddedMediaPlayerComponent getPlayerComponent() {
		return mediaPlayerComponent;
	}

	public boolean loadVideo(String path) {
		return player.playMedia(path);
	}

	public void pause() {
		if (player.isPlaying()) {
			player.pause();
		}
	}

	public void play() {
		if (player.isPlayable()) {
			player.play();
		}
	}

}
