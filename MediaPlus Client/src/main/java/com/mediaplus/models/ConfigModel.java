package com.mediaplus.models;

import com.mediaplus.BaseObject;
import com.mediaplus.types.HomeCategoryType;
import com.mediaplus.types.HomeLayoutType;

public class ConfigModel extends Model {
	public ConfigModel() {
		super();
	}
	
	// gets
	public String getIp() {
		return BaseObject.pref.getServerIp();
	}
	
	public int getPort() {
		return BaseObject.pref.getServerPort();
	}
	
	public String getPortTxt() {
		return String.valueOf(getPort());
	}
	
	public boolean getFullScreen() {
		return BaseObject.pref.getFullScreen();
	}
	
	public int getLayoutId() {
		return getLayout().id();
	}
	
	public int getCategoryId() {
		return getCategory().id();
	}
	
	// sets
	public void setIp(String ip) {
		BaseObject.pref.setServerIp(ip);
		saveConfig();
	}
	
	public void setPort(int port) {
		BaseObject.pref.setServerPort(port);
		saveConfig();
	}

	public void setFullScreen(boolean fullScreen) {
		BaseObject.pref.setFullScreen(fullScreen);
		saveConfig();
	}
	
	public void setLayoutId(int id) {
		setLayout(id);
	}

	public void setCategoryId(int id) {
		setCategory(id);
	}

	// private
	// gets
	private HomeLayoutType getLayout() {
		return BaseObject.pref.getLayout();
	}
	
	private HomeCategoryType getCategory() {
		return BaseObject.pref.getCategory();
	}
	
	// sets
	private void setCategory(int id) {
		BaseObject.pref.setCategory(HomeCategoryType.get(id));
		saveConfig();
	}
	
	private void setLayout(int id) {
		BaseObject.pref.setLayout(HomeLayoutType.get(id));
		saveConfig();
	}
	
	// other
	private boolean saveConfig() {
		return BaseObject.pref.save();
	}
}
